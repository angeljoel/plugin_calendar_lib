 

 
const icons_sub_menu = {
  description: '<i style="font-size: 11px" class="material-icons mt-1">laptop</i>',
 modality: '<i style="font-size: 11px" class="material-icons mt-1">laptop</i>',
 place: '<i style="font-size: 11px" class="material-icons mt-1">place</i>',
 alert:  '<i style="font-size: 17px" class="material-icons mt-1" >notifications_active</i>', 
 time: '<i style="font-size: 11px" class="material-icons mt-1">access_time</i>',
 academic_coordination_person: '<i style="font-size: 11px" class="material-icons mt-1" >person</i>',  
 academic_coordination_phone: '<i style="font-size: 11px" class="material-icons mt-1" >phone_in_talk</i>', 
 academic_coordination_email:'<i style="font-size: 11px" class="material-icons mt-1" >email</i>', 
 help_desk_person:'<i style="font-size: 11px" class="material-icons mt-1">person</i>', 
 help_desk_phone: '<i style="font-size: 11px" class="material-icons mt-1">phone_in_talk</i>',  
};

// Color al azar 
  const colors = [
    {
      color: '#D86450',
      background: '#FDF4F2',
      color_title: '#D4523C'
    },
    {
      color: '#FFC001',
      background: '#FFFAEB',
      color_title: '#FFCE3B'
    },
     {
      color: '#49ADF0',
      background: '#F0F9FE',
      color_title: '#53B2F1'
    },
     {
      color: '#139239',
      background: '#ECF6EF',
      color_title: '#48AB65'
    },
    {
      color: '#D86450',
      background: '#FDF4F2',
      color_title: '#D4523C'
    },
    {
      color: '#FFC001',
      background: '#FFFAEB',
      color_title: '#FFCE3B'
    },
     {
      color: '#49ADF0',
      background: '#F0F9FE',
      color_title: '#53B2F1'
    },
     {
      color: '#139239',
      background: '#ECF6EF',
      color_title: '#48AB65'
    },
    {
      color: '#49ADF0',
      background: '#F0F9FE',
      color_title: '#53B2F1'
    },
]
//fin de color al azar

const templateCard = ` 
<div   @click.prevent="getMenu"
:style="getStyle">
<div style="align-self: center;">
<div :class="[index == 1 ? 'text-uppercase font-title  header-desktop-title color-count-title' : 
                          'text-uppercase font-title  header-desktop-title ' ]"> 
      <span class="text-title-card-overflow break-word" 
      :style="{'color': color}" v-html="param.title"></span>
      <div v-show="more_than_two && index == 1" class="center-count">+{{count}}</div>
</div>
<div class="header-desktop-text">
  <div class="label-body-date break-word "  >
    <div style="line-height:0.8rem"> {{param.text}} </div>
    <div  class="center-count  "
    v-show="more_than_two && index == 1">+{{count}}</div>
</div>
</div>
<div class="header-desktop-hours" style="font-size:10px">{{param.star_hours}}</div>
</div>                    
</div>
`

const templateSubMenuList = `
<div>
<div style="display:flex;justify-content:flex-end;padding-top:0.8rem " class="plr08">
  <button class="btn btn-sm mt-0 p-0">
    <i
      class="material-icons mt-0 p-0 color-icon"
      style="font-size:13px;  "
      @click.prevent="setEstate()"
    >close</i>
  </button>
</div>

<div :style="getStyleMovil" class="scroll-global">
  <h6
    class="mt-0 mb-0 text-uppercase plr08 "
    :style="{'font-size':'10px', 'color': color_title}"
    v-html="data.title"
  ></h6>
  <div class="font-weight-bold plr08  ">{{data.text}}</div>
  <div class="font-weight-bold plr08" style="font-size:12px;margin-top:1rem">DESCRIPCIÓN</div>
  <hr class="separator mlr08" style="margin-top:0.3rem" />
  <div class="media position-relative pt-1 plr08">
    <div :style="{'color':color_title}"  

    v-html="icons_sub_menu.description"></div>

    <div class="media-body" style="padding-left:0.4rem">
      <div class="mt-0 font-weight-normal font-text" style="font-size:13px">{{data.description}}</div>
    </div>
  </div>

  <div class="font-weight-bold  plr08" style="font-size:12px;margin-top:1rem">
    MODALIDAD
    <span class="text-uppercase">{{data.modality}}</span>
  </div>
  <hr class="separator mlr08" style="margin-top:0.3rem" />
  <div v-show="data.modality  == 'web' " class="media position-relative pt-1 plr08">
    <div :style="{'color':color_title}" v-html="icons_sub_menu.modality"></div>

    <div class="media-body" style="padding-left:0.4rem">
      <div class="mt-0" style="font-size:13px">{{data.platform}}</div>
    </div>
  </div>  

  <div v-show="user == 'D' || user == 'A' || user == 'C' " >
    <div class="media position-relative pt-1  plr08"  v-show="data.modality != 'web'">
      <div :style="{'color':color_title}" v-html="icons_sub_menu.place"></div>

      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.place}} <span v-show="user == 'A'">{{data.classroom}}</span> </div>
      </div>
      <br />
    </div>
    <div class="media position-relative  plr08"  v-show="user == 'D' || user == 'C' ">
      <div class="media-body">
        <div class="space-between">
          <div class="space-normal" style="width:70%">
            <input type="text" class="form-control form-control-sm" 
              
            v-model="input_save" />
            <div
              :style="{'font-size': '16px','background':sub_menu_background , 'color':'#FFF' 
                   }"
              class="  ml-2 btn-save"
              @click.prevent="save()"
            >
              <span :style="{'color': color_title , 'font-weight': 500 , 'font-size': '14px',
                }" class="p-0">Guardar</span>
            </div>
          </div>
          <div>
            <div
              @click.prevent="alert()"
              :style="{'background': color_title }"
              class="btn-alert ml-2"
            >
              <div style="color: #FFF;" v-html="icons_sub_menu.alert"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="font-weight-bold plr08" style="font-size:13px;margin-top:1rem">HORARIO</div>
  <hr class="separator mlr08" style="margin-top:0.3rem" />
  <div class="media position-relative pt-1  plr08">
    <div :style="{'color':color_title}" v-html="icons_sub_menu.time"></div>

    <div class="media-body" style="padding-left:0.4rem">
      <div class="mt-0" style="font-size:13px">
        {{data.date_name}}
        <br />
        {{data.star_hours}} - {{data.end_hours}}
      </div>
    </div>
  </div>

  <div v-show="user == 'D' || user == 'A' || user == 'C'">
    <div class="font-weight-bold plr08" style="font-size:12px;margin-top:1rem">COORDINACIÓN ACADÉMICA</div>
    <hr class="separator mlr08" style="margin-top:0.3rem" />
    <div class="media position-relative pt-1  plr08">
      <div
        :style="{'color':color_title}"
        v-html="icons_sub_menu.academic_coordination_person"
      ></div>
      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.academic_coordination_person}}</div>
      </div>
      <br />
    </div>

    <div class="media position-relative  plr08">
      <div
        :style="{'color':color_title}"
        v-html="icons_sub_menu.academic_coordination_phone"
      ></div>

      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.academic_coordination_phone}}</div>
      </div>
      <br />
    </div>

    <div class="media position-relative  plr08">
      <div
        :style="{'color':color_title}"
        v-html="icons_sub_menu.academic_coordination_email"
      ></div>

      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px"><u>{{data.academic_coordination_email}}</u></div>
      </div>
      <br />
    </div>
  </div>
  <div v-show="user == 'D' || user == 'A' || user == 'C'">
    <div class="font-weight-bold plr08" style="font-size:12px;margin-top:1rem">MES DE AYUDA</div>
    <hr class="separator mlr08" style="margin-top:0.3rem" />
    <div class="media position-relative pt-1 plr08">
      <div :style="{'color':color_title}" v-html="icons_sub_menu.help_desk_person"></div>
      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.help_desk_person}}</div>
      </div>
      <br />
    </div>
    <div class="media position-relative  plr08">
      <div :style="{'color':color_title}" v-html="icons_sub_menu.help_desk_phone"></div>
      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.help_desk_phone}}</div>
      </div>
      <br />
    </div>
  </div>
</div>
</div>
`

const templateSubMenu = `   <div>
<div style="display:flex;justify-content:flex-end;padding-top:0.8rem " class="plr08">
  <button class="btn btn-sm mt-0 p-0">
    <i
      class="material-icons mt-0 p-0 color-icon"
      style="font-size:13px;  "
      @click.prevent="setEstate()"
    >close</i>
  </button>
</div>

<div :style="getStyleMovil" class="scroll-global">
  <h6
    class="mt-0 mb-0 text-uppercase plr08 "
    :style="{'font-size':'10px', 'color': color_title}"
    v-html="data.title"
  ></h6>
  <div class="font-weight-bold plr08  ">{{data.text}}</div>
  <div class="font-weight-bold plr08" style="font-size:12px;margin-top:1.2rem">DESCRIPCIÓN</div>
  <hr class="separator mlr08" style="margin-top:0.3rem" />
  <div class="media position-relative pt-1 plr08">
    <div :style="{'color':color_title}"  

    v-html="icons_sub_menu.description"></div>

    <div class="media-body" style="padding-left:0.4rem">
      <div class="mt-0 font-weight-normal font-text" style="font-size:13px">{{data.description}}</div>
    </div>
  </div>

  <div class="font-weight-bold   plr08" style="font-size:13px;margin-top:1rem">
    MODALIDAD
    <span class="text-uppercase">{{data.modality}}</span>
  </div>
  <hr class="separator mlr08" style="margin-top:0.3rem" />
  <div v-show="data.modality  == 'web' " class="media position-relative pt-1 plr08">
    <div :style="{'color':color_title}" v-html="icons_sub_menu.modality"></div>

    <div class="media-body" style="padding-left:0.4rem">
      <div class="mt-0" style="font-size:13px">{{data.platform}}</div>
    </div>
  </div>

  <div v-show="user == 'D' || user == 'A' || user == 'C'  " >
    <div class="media position-relative pt-1  plr08" v-show="data.modality != 'web'">
      <div :style="{'color':color_title}" v-html="icons_sub_menu.place"></div>

      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.place}} <span v-show="user == 'A'">
          {{data.classroom}}</span> </div>
      </div>
      <br />
    </div>
    <div class="media position-relative  plr08"  v-show="user == 'D' || user == 'C' ">
      <div class="media-body">
        <div class="space-between">
          <div class="space-normal" style="width:70%">
            <input type="text" class="form-control form-control-sm" 
              
            v-model="input_save" />
            <div
              :style="{'font-size': '16px','background':sub_menu_background , 'color':'#FFF' 
                   }"
              class="  ml-2 btn-save"
              @click.prevent="save()"
            >
              <span :style="{'color': color_title , 'font-weight': 500 , 'font-size': '14px',
                }" class="p-0">Guardar</span>
            </div>
          </div>
          <div>
            <div
              @click.prevent="alert()"
              :style="{'background': color_title }"
              class="btn-alert ml-2"
            >
              <div style="color: #FFF;" v-html="icons_sub_menu.alert"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="font-weight-bold  plr08" style="font-size:13px;margin-top:1rem">HORARIO</div>
  <hr class="separator mlr08" style="margin-top:0.3rem" />
  <div class="media position-relative pt-1  plr08">
    <div :style="{'color':color_title}" v-html="icons_sub_menu.time"></div>

    <div class="media-body" style="padding-left:0.4rem">
      <div class="mt-0" style="font-size:13px">
        {{data.date_name}}
        <br />
        {{data.star_hours}} - {{data.end_hours}}
      </div>
    </div>
  </div>

  <div v-show="user == 'D' || user == 'A' || user == 'C' ">
    <div class="font-weight-bold plr08" style="font-size:12px;margin-top:1rem">COORDINACIÓN ACADÉMICA</div>
    <hr class="separator mlr08" style="margin-top:0.3rem" />
    <div class="media position-relative pt-1  plr08">
      <div
        :style="{'color':color_title}"
        v-html="icons_sub_menu.academic_coordination_person"
      ></div>
      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.academic_coordination_person}}</div>
      </div>
      <br />
    </div>
    <div class="media position-relative  plr08">
      <div
        :style="{'color':color_title}"
        v-html="icons_sub_menu.academic_coordination_phone"
      ></div>
      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.academic_coordination_phone}}</div>
      </div>
      <br />
    </div>
    <div class="media position-relative  plr08">
      <div
        :style="{'color':color_title}"
        v-html="icons_sub_menu.academic_coordination_email"
      ></div>
      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px"><u>{{data.academic_coordination_email}}</u></div>
      </div>
      <br />
    </div>
  </div>

  <div v-show="user == 'D' || user == 'A' || user == 'C'">
    <div class="font-weight-bold plr08" style="font-size:12px;margin-top:1rem">MES DE AYUDA</div>
    <hr class="separator mlr08" style="margin-top:0.3rem" />
    <div class="media position-relative pt-1 plr08">
      <div :style="{'color':color_title}" v-html="icons_sub_menu.help_desk_person"></div>
      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.help_desk_person}}</div>
      </div>
      <br />
    </div>
    <div class="media position-relative  plr08">
      <div :style="{'color':color_title}" v-html="icons_sub_menu.help_desk_phone"></div>
      <div class="media-body" style="padding-left:0.4rem">
        <div class="mt-0" style="font-size:13px">{{data.help_desk_phone}}</div>
      </div>
      <br />
    </div>
  </div>
</div>
</div>`

    
const templateCardWeek = 
                      ` <div   @click.prevent="getMenu"
                      class="card-vertical"
                      :style="getStyle"   >
                        
                    <div style="align-self: center;">
                      
                     <div :class="[index == 1 ? 'text-uppercase font-title  header-desktop-title color-count-title' : 
                                      'text-uppercase font-title  header-desktop-title ']" 
                          
                           > 
                             <span :style="{'color': param.color_title}" v-html="param.title"
                            
                              ></span>
                               
                            <div v-show="more_than_two && index == 1" class="center-count  "
                           >+{{count}}</div>
                           
                      </div>
                      <div class="header-desktop-text">
                        <div class="label-body-date break-word " >
                          {{param.text}}
                          <div  class="center-count  "
                          v-show="more_than_two && index == 1">+{{count}}</div>
                      </div>
                      </div>
                      <div class="header-desktop-hours" style="font-size:10px">{{param.star_hours}}</div>
                  </div>
                       
            </div>
   `



                      const templateMenu = 
                      ` <div>
                      <div>
                           <div class="container-menu space-between">
                                 <span>{{data.date_name}}</span>
                                 <button class="btn btn-sm  mt-0 p-0" >
                                  <i
                                  class="material-icons mt-0 p-0 color-icon btn-menu-cancel"
                                  style="font-size:13px;  "
                                   @click.prevent="cancelMenu(); state_menu = false"
                                  >close</i>
                              </button>
                                
          
                              </div>
                              <hr class="separator">
                              <div  class="scroll-global body-menu list-menu" :style="getStyleMovil" >
                                      <template v-for="(param,index_body) in data">
                                              <div :key="'menu'+ index_body">
                                                   <div  class="pb-2">
                                                  <div class="media position-relative pt-2" v-if="param.sub_title">
                                                  <i :style="{'font-size':'8px','padding-top':'0.45rem' ,'color': param.color_title}"  class="material-icons mr-2">
                                                   lens
                                                  </i>
                                                  <div class="media-body">
                                                    <h6 class="mt-0 " style="font-size:14px;font-weight:700">{{ param.sub_title }}</h6>
                                                  </div>
                                                  </div>
                                                  <div class="cursor-pointer " >
                                                     <div @click.prevent="setSubMenu(para_data)" 
                                                           v-for="(para_data,index_data) in param.data" :key="index_data+ 'data'"
                                                          class="pt-2 hover-sub-menu">
                                                      <h6 class="mt-0 mb-0 text-uppercase" :style="{'font-size':'10px' , 'color': para_data.color_title}" 
                                                          v-html="para_data.title"> </h6>
                                                      <div class="font-weight-bold title-sub-menu" style="font-size:13px">{{para_data.text}}</div>
                                                      <div  style="font-size:12px">
                                                        {{para_data.star_hours}} - {{para_data.end_hours}} - {{para_data.place}}
                                                        </div> 
                                                        <hr v-show="index_data +1 < param.data.length" class="separator" style="margin-top:0.3rem" />
                                                    </div>     
                                                  </div>
                                               </div>
                                               
                                              </div>
                                     </template>
                              </div>
                          </div>
                                  <sub-menu 
                                     :user="user"
                                     @saveSubMenu="saveSubMenu"
                                     @alertSubMenu="alertSubMenu"
                                      :color_title ="color_title"
                                     :icons_sub_menu ="icons_sub_menu"
                                     :sub_menu_color="sub_menu_color"
                                      :sub_menu_background="sub_menu_background"
                                      v-if="state_menu"   
                                     :style="getStyle"
                                     :data="sub_data"
                                     @setEstate="state_menu = false" />
                                     
                                       
          </div>`




                      const templateList = 
                      `     <div>
                      <div  v-show="data.length > 0" v-for="(param,index) in  data" 
                            class="wrapper center-block pl-2 pr-2" :key="index + 'list'" 
                           >
                        <div
                         v-show="param.type == 'P'"
                          class="panel-group"
                          style="margin-top:2rem"
                          id="accordion"
                          role="tablist"
                          aria-multiselectable="true"
                         
                          >
                          <div class="panel panel-default">
                            <div role="tab" id="headingOne">
                              <a style="text-decoration: none !important; text-decoration-line:none"
                                 @click.prevent="setIcon('list-'+ index)"
                                role="button"
                                data-toggle="collapse"
                                data-parent="#accordion"
                                :href="'#collapse' + index"
                                aria-expanded="true"
                                :aria-controls="'#collapse' + index"
                              >
                                <!-- header -->
                                  <table style="width: 100%;table-layout: fixed;">
                                      <tbody>
                                          <tr>
                                            <td style="width: 50%;color:black;">
                                                <div class="text-uppercase " v-show="param.type_text" style="font-weight: 700;line-height: normal;font-size:12px">{{param.type_text}}</div>
                                                <div style="font-size:16px;font-weight: 700;" class="title-sub-menu" :title="param.program">{{ param.program }}</div>
                                            </td>
                                            <td style="width: 19%;">
                                                <div style="color:black;font-size:13px" v-show="param.academic_coordination_person">
                                                    Cordinador: <span class="font-weight-bold">{{param.academic_coordination_person}}</span>
                                                </div>
                                            </td>
                                            <td style="width: 9%;">
                                            </td>
                                            <td style="width: 22%;">
                                                <table style="width: 100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 90%;color:black;font-size: 13px;">
                                                                Inicio:
                                                                <span style="font-weight: 700;">{{param.program_dstart}}</span> - Fin
                                                                <span style="font-weight: 700;">{{param.program_dend}}</span>
                                                            </td>
                                                            <td style="width: 10%;">
                                                                <i style="color: black;position: abosolute" :id="'list-'+index"
                                                                    class="material-icons panel-heading">expand_more</i>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                          </tr>
                                      </tbody>
                                  </table>
                                <!-- end header -->
                              </a>
                            </div>
                            <div class="border-bottom-list"></div>
                            <!-- body -->
                            <div
                              :id="'collapse' + index"
                              class="panel-collapse collapse in"
                              role="tabpanel"
                              aria-labelledby="headingOne"
                            >
                             <template v-for="(body_data,index_body) in param.courses">
                              <div class="panel-body" :key="index_body+ 'body-list'" 
                              >
                                <div
                                :href="'#collapseOne' +index+index_body" 
                                data-toggle="collapse"
                                data-parent="#accordion"
                                   class="wrapper center-block mt-2 mb-2 pl-2 header-body " 
                                   :style="{'border-left': '0.2rem solid ' + body_data.color ,'background-color':body_data.background }"
                                  >
                                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                      <div role="tab" id="headingOne">
                                        <a
                                          @click.prevent="setIcon('list-body-'+index+index_body)"
                                          style="text-decoration: none !important;text-decoration-line:none"
                                          role="button"
                                          :href="'#collapseOne' +index+index_body"
                                          aria-expanded="true"
                                          aria-controls="collapseOne"
                                        >
                                          <!-- header -->
                                            <table style="width: 100%;table-layout: fixed;">
                                              <tbody>
                                                  <tr>
                                                      <td style="width: 57%;">                
                                                          <div style="color:black;font-size:14px;" class="font-weight-bold title-sub-menu" :title="body_data.sub_title">
                                                                  {{ body_data.sub_title }}</div>
                                                      </td>
                                                      <td style="width: 12%;">
                                                          <div style="color:black;font-size:13px;">
                                                              Modalidad: <span class="font-weight-bold">{{body_data.modality}}</span>
                                                          </div>
                                                      </td>
                                                      <td style="width: 9%;">
                                                          <div style="color:black;font-size:13px">
                                                              Sesiones: <span class="font-weight-bold">{{body_data.sessions}}</span>
                                                          </div>
                                                      </td>
                                                      <td style="width: 22%;">
                                                          <table style="width: 100%;">
                                                              <tbody>
                                                                  <tr>
                                                                      <td style="width: 90%;color:black;font-size: 13px;">
                                                                          Inicio:
                                                                          <span style="font-weight: 700;">{{body_data.course_dstart}}</span> - Fin
                                                                          <span style="font-weight: 700;">{{body_data.course_dend}}</span>
                                                                      </td>
                                                                      <td style="width: 10%;">
                                                                          <i :style="{'color': body_data.color , 'margin-top': '0.05rem'}" :id="'list-body-'+index+index_body"
                                                                          class="material-icons panel-heading">expand_more</i>
                                                                      </td>
                                                                  </tr>
                                                              </tbody>
                                                          </table>
                                                      </td>
                                                  </tr>
                                              </tbody>
                                          </table>
                                                      <!-- end header -->
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                  
                                </div>
                                      <div
                                        :id="'collapseOne' +index+index_body"
                                        style="background:white"
                                        class="panel-collapse collapse "
                                        role="tabpanel"
                                        aria-labelledby="headingOne"
                                       
                                      >
                                        <div class="font-list font-list-border" style="position:relative">
                                          <div v-for="(event,index_item) in body_data.events" 
                                                :key="'event' + index_item" class="hover-sub-menu pl-3 pb-2 pt-2" 
                                                @click.prevent="getMenu($event,event)"
                                              >
                                              <table style="width: 100%;table-layout: fixed;">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 19%;">
                                                            <div> {{event.name}}, {{event.dateD}} de {{event.name_month}} de {{event.dateY}}</div>
                                                        </td>
                                                        <td style="width: 19%;">
                                                            <div>{{event.star_hours}} - {{event.end_hours}}</div>
                                                        </td>
                                                        <td style="width: 19%;">
                                                            <div :style="{color:event.color_title, 'font-size': '10px'}" v-html="event.title"></div>
                                                            <div class="title-sub-menu" :title="event.text">{{event.text}}</div>
                                                        </td>
                                                        <td style="width: 12%;">
                                                            <div v-show="event.modality">
                                                                Modalidad: <span class="font-weight-bold">{{event.modality}}</span>
                                                            </div>
                                                        </td>
                                                        <td style="width: 9%;">
                                                            <div v-show="event.classroom" class="title-sub-menu" :title="event.classroom">
                                                                Aula: <span class="font-weight-bold">{{ event.classroom}}</span>
                                                            </div>
                                                        </td>
                                                        <td style="width: 22%;">
                                                            <div v-show="event.docente" class="title-sub-menu" :title="event.docente">
                                                                Docente: <span class="font-weight-bold">{{event.docente}}</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                          </div>
                                      </div>
                  
                                      </div>
                              </div>
                             </template>
                            </div>
                             <!-- end body -->
                          </div>
                        </div>
                        <div  v-if="param.type == 'C'"> 
                              <div class="panel-body" :key="index+ 'body-list-cursour'" 
                              >
                                <div
                                  :href="'#collapseOneCourse' +index" 
                                                          data-toggle="collapse"
                                          data-parent="#accordion"
                                     
                                          aria-expanded="true"
                                          :aria-controls="'#collapseOneCourse' + index"
                                  class="wrapper center-block mt-2 mb-2 pl-2 header-body "  
                                  :style="{'border-left': '0.2rem solid ' + param.color  ,'background-color': param.background }"
                                  >
                                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                      <div role="tab" id="headingOne">
                                        <a
                                          @click.prevent="setIcon('body-list-cursour-'+index)"
                                          style="text-decoration: none !important;text-decoration-line:none"
                                          role="button"
                                        >
                  
                                          <!-- header -->                                          
                   
                                          <table style="width: 100%;table-layout: fixed;">
                                              <tbody>
                                                  <tr>
                                                      <td style="width: 57%;">
                                                          <div v-show="param.type_text" class="text-uppercase" style="font-weight:700;font-size:10px;line-height: normal;">{{param.type_text}}</div>
                                                          <div :title="param.sub_title" class="title-sub-menu" style="font-size: 14px;font-weight: 700;">{{ param.sub_title }}</div>
                                                      </td>
                                                      <td style="width: 12%;font-size:13px;">
                                                          <div style="color:black;">Modalidad: <span class="font-weight-bold" style="text-transform: capitalize;">{{param.modality}}</span></div>
                                                      </td>
                                                      <td style="width: 9%;font-size:13px">
                                                          <div style="color:black;">Sesiones: <span class="font-weight-bold">{{param.sessions}}</span></div>
                                                      </td>
                                                      <td style="width: 22%;">
                                                          <table style="width: 100%;">
                                                              <tbody>
                                                                  <tr>
                                                                      <td style="width: 90%;font-size: 13px;">
                                                                          Inicio: <span style="font-weight:700;">{{param.course_dstart}}</span> - Fin <span style="font-weight:700;">{{param.course_dend}}</span>
                                                                      </td>
                                                                      <td style="width: 10%;">
                                                                          <i :style="{'color': param.color , 'margin-top': '0.05rem'}"
                                                                              :id="'body-list-cursour-'+index "
                                                                              class="material-icons panel-heading">expand_more</i>
                                                                      </td>
                                                                  </tr>
                                                              </tbody>
                                                          </table>
                                                      </td>
                                                  </tr>
                                              </tbody>
                                          </table>

                                          <!-- end header -->
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                  
                                 <div class="border-bottom-list" v-show="index +1 < data.length"></div>
                                 <div
                                        :id="'collapseOneCourse' +index "
                                        style="background:white"
                                        class="panel-collapse collapse in "
                                        role="tabpanel"
                                        aria-labelledby="headingOne"
                                      >                                      
                                      <div class="font-list font-list-border " style="position:relative">
                                        <div v-for="(event,index_item) in param.events" 
                                            :key="'event' + index_item" class="hover-sub-menu  pl-3 pb-2 pt-2" 
                                            @click.prevent="getMenu($event,event)">
                                            <table style="width: 100%;table-layout: fixed;">
                                              <tbody>
                                                  <tr>
                                                      <td style="width: 19%;">
                                                          <div> {{event.name}}, {{event.dateD}} de {{event.name_month}} de {{event.dateY}}</div>
                                                      </td>
                                                      <td style="width: 19%;">
                                                          <div>{{event.star_hours}} - {{event.end_hours}}</div>
                                                      </td>
                                                      <td style="width: 19%;">
                                                          <div :style="{color:event.color_title, 'font-size': '10px'}" v-html="event.title"></div>
                                                          <div class="title-sub-menu" :title="event.text">{{event.text}}</div>
                                                      </td>
                                                      <td style="width: 12%;">
                                                          <div v-show="event.modality">
                                                              Modalidad: <span class="font-weight-bold" style="text-transform: capitalize;">{{event.modality}}</span>
                                                          </div>
                                                      </td>
                                                      <td style="width: 9%;">
                                                          <div v-show="event.classroom" class="title-sub-menu" :title="event.classroom">
                                                              Aula: <span class="font-weight-bold">{{ event.classroom}}</span>
                                                          </div>
                                                      </td>
                                                      <td style="width: 22%;">
                                                          <div v-show="event.docente" class="title-sub-menu" :title="event.docente">
                                                              Docente: <span class="font-weight-bold">{{event.docente}}</span>
                                                          </div>
                                                      </td>
                                                  </tr>
                                              </tbody>
                                            </table>                                                   
                                          </div>
                                        </div>       
                                      </div>
                  
                   
                              </div>
                            
                            </div>
                      </div> 
                      <div v-show="data.length == 0" v-html="empty_messague">
                        
                      </div>
                       
                              <div        :style="getStyle"
                                           v-if="state_menu">
                                           <sub-menu 
                                             :color_title ="color_title"
                                             :icons_sub_menu ="icons_sub_menu"
                                             :sub_menu_color="color"
                                             :sub_menu_background="background"
                                             @saveSubMenu="saveSubMenu"
                                             @alertSubMenu="alertSubMenu"
                                             :data="menu_data"
                                             @setEstate="state_menu = false"
                                             :user="user" />
                                                  
                          </div>
                    </div>
                      `

                        
const templateCalendar = 
                          `    <div>
                          <div class="card m-1 padding-left-right">
                            <div>
                              <!-- Header -->
                              <div class="header-desktop">
                                <div class="row">
                                  <div class="col-lg-4 col-md-4 col-sm-12 center-movil" style="padding-right:0">
                                    <button
                                      @click.prevent="setDay()"
                                      type="button"
                                      class="btn btn-md div-border d-inline"
                                      style="margin-right:1rem"
                                    >Hoy</button>
                                    <button
                                      @click.prevent="setMonth(false)"
                                      type="button"
                                      class="btn btn-xs d-inline"
                                      style="padding:0"
                                    >
                                      <i class="material-icons font-size-ico" style="font-size:30px">keyboard_arrow_left</i>
                                    </button>
                                    <button
                                      @click.prevent="setMonth(true)"
                                      type="button"
                                      class="btn btn-sm d-inline"
                                      style="padding:0"
                                    >
                                      <i class="material-icons font-size-ico" style="font-size:30px">keyboard_arrow_right</i>
                                    </button>
                      
                                    <label for class="ml-4 font-label label-month" v-text="month_label"></label>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-12 inputSearch mt-movil" v-show="view_search">
                                    <input
                                      type="search"
                                      v-model="search"
                                      class="form-control background-search form-control-md d-inline"
                                      style="border:none;font-size:14px;width:100%"
                                      placeholder="Buscar Coordinadores, Diplomas, Cursos, Seminarios..."
                                    />
                                  </div>
                                  <div
                                    class="col-lg-4 col-md-4 col-sm-12 center-movil-right mt-movil"
                                    style="padding-left:0;text-align: right;"
                                  >
                                    <div class="d-inline">
                                      <label v-show="view_month && view_week" for class="font-label mr-2">Vistas:</label>
                                      <button
                                        v-show="view_month"
                                        type="button"
                                        class="btn btn-sm mr-1"
                                        @click.prevent="setType('month')"
                                        :class="[ mode == 'month' ? icon_color_header : 'div-border']"
                                      >
                                        <i class="material-icons font-size-ico">calendar_today</i>
                                      </button>
                                      <button
                                        v-show="view_week"
                                        type="button"
                                        class="btn btn-sm mr-4"
                                        @click.prevent="setType('list')"
                                        :class="[ mode == 'list' ? icon_color_header : 'div-border']"
                                      >
                                        <i class="material-icons font-size-ico">list</i>
                                      </button>
                      
                      
                                  
                                      <button
                                        type="button"
                                        class="form-control form-control-md d-inline font-label vertical-align"
                                        style="max-width:7rem ; "
                                        data-toggle="dropdown"
                                        data-display="static"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        
                                      >
                                      <div class="space-between">
                                          {{type == 'month'  ? 'Mes' : 'Semana'}}
                                           <i class="material-icons">
                                      arrow_drop_down
                                      </i>
                                      </div>
                      
                                      
                                      </button>
                                     
                                      <div
                                        class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left"
                                        style=" margin-left:9rem;top:-1rem;padding:8px 0 8px 0;width:10rem"
                                      >
                                        <a
                                          class="dropdown-item fw-500"
                                          @click.prevent="onChangeType('month'); type = 'month'"
                                          href="#"
                                        >Mes</a>
                                        <a
                                          class="dropdown-item fw-500"
                                          @click.prevent="onChangeType('week'); type = 'week'"
                                          href="#"
                                        >Semana</a>
                                       
                                      </div>
                                    
                      
                                      <!-- <select
                                        v-if="view_select"
                                        class="form-control form-control-md d-inline font-label vertical-align"
                                        style="max-width:5.5rem ; "
                                        v-model="type"
                                        @change="onChangeType(type)"
                                      >
                                        <option selected value="month">Mes</option>
                                        <option value="week">Semana</option>
                                      </select> -->
                                    </div>
                      
                                    <button
                                      v-if="view_synchronize"
                                      @click.prevent="synchronizeCalendar()"
                                      type="button"
                                      :class="['btn btn-md font-label d-inline background-search  pl-2 ml-4 mt-movil btn-calendar ' + icon_color_header]"
                                      style="border: none"
                                    >Sincronizar Calendario</button>
                                  </div>
                                </div>
                              </div>
                              <div class="header-movil">
                                <div class="space-between">
                                  <div>
                                    <button class="btn btn-md" @click.prevent="getMenuBtn()">
                                      <i class="material-icons">menu</i>
                                    </button>
                                    <label for class="font-label label-month" v-text="  month_label "></label>
                                  </div>
                      
                                  <div>
                                    <div class="btn-group">
                                      <button
                                        type="button"
                                        class="btn btn-md"
                                        data-toggle="dropdown"
                                        data-display="static"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                      >
                                        <i class="material-icons">more_horiz</i>
                                      </button>
                                      <div
                                        class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left"
                                        style=" right:0.5rem;top:0.5rem;padding:8px 0 0 0"
                                      >
                                        <a
                                          class="dropdown-item fw-500"
                                          @click.prevent="onChangeType('month'); type = 'month'"
                                          href="#"
                                        >Mes</a>
                                        <a
                                          class="dropdown-item fw-500"
                                          @click.prevent="onChangeType('week'); type = 'week'"
                                          href="#"
                                        >Semana</a>
                                        <div class="dropdown-divider" style="margin:0;margin-top:8px"></div>
                                        <a class="dropdown-item a-calendar" href="#">Agregar a calendario</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- end Header -->
                      
                              <!-- Body -->
                              <transition name="fade" mode="out-in">
                                <div
                                  v-hammer:tap="onTap"
                                  v-hammer:pan.horizontal="onPanHorizontal"
                                  v-hammer:panstart="onPanStart"
                                  v-hammer:panend="onPanEnd"
                                  v-hammer:press="onPress"
                                  v-hammer:pressup="onPressup"
                                  v-if="mode == 'month'"
                                  style="position:relative"
                                >
                                  <transition name="fade" mode="out-in">
                                    <div v-if="type == 'month'" key="1">
                                      <div class="container-dates-label" style="margin-top:0.7rem">
                                        <template v-for="elem in days_array_names">
                                          <div :key="elem.name" class="header-desktop">
                                            <div class="text-uppercase font-date-label">{{elem.nameLg}}</div>
                                          </div>
                                          <div :key="elem.name" class="header-movil">
                                            <div class="text-uppercase font-date-label">{{elem.nameXs}}</div>
                                          </div>
                                        </template>
                                      </div>
                                      <div
                                        :class="[ !state_data ? 'container-dates disable-div' : 'container-dates block-div']"
                                      >
                                        <div
                                          v-for="(elem,index)  in days_array"
                                          :key="index"
                                          :class="[(current_day == elem.date) ? 'border-dates-day' : 'border-dates']"
                                          :style="{color: elem.color , background: elem.background}"
                                        >
                                          <img
                                             
                                            v-show="current_day == elem.date"
                                             src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMzFweCIgaGVpZ2h0PSI3MXB4IiB2aWV3Qm94PSItMC41IC0wLjUgMzEgNzEiIGNvbnRlbnQ9IiZsdDtteGZpbGUgaG9zdD0mcXVvdDt3d3cuZHJhdy5pbyZxdW90OyBtb2RpZmllZD0mcXVvdDsyMDIwLTAxLTIzVDIxOjIwOjE4Ljc5OFomcXVvdDsgYWdlbnQ9JnF1b3Q7TW96aWxsYS81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzc5LjAuMzk0NS4xMzAgU2FmYXJpLzUzNy4zNiZxdW90OyBldGFnPSZxdW90O29CSmptZDN0dzlPcHVQQTVsdVlYJnF1b3Q7IHZlcnNpb249JnF1b3Q7MTIuNS42JnF1b3Q7Jmd0OyZsdDtkaWFncmFtIGlkPSZxdW90O09hYmhOaWEwNGVvNDBuTVJzWXRVJnF1b3Q7IG5hbWU9JnF1b3Q7UMOhZ2luYS0xJnF1b3Q7Jmd0O2paTkxiNE13RElCL0RjZE5RS0R0cnFQdGR0aE9sVHFwdHdoY2tpMWdGTnhCKytzWGlpbXdoN1FUOXVkbmJPT0pwR2lmckt6VUsyWmd2TkRQV2src3ZUQU1oSGh3bjQ2Y2U3S0k0aDdrVm1mc05JS2R2Z0JEbitsSloxRFBIQW5Sa0s3bU1NV3loSlJtVEZxTHpkenRpR1pldFpJNS9BQzdWSnFmOUUxbnBIcTZpdjJSUDRQTzFWQTU4TmxTeU1HWlFhMWtoczBFaVkwbkVvdEl2VlMwQ1podWVNTmMrcmp0SDFadXJLYnowT3JRa1lXUy9wTmh2MSsvNHo0OWJCWEFvYmtFTDNWTWQ1ejJVNXJUTE8ya0Rsa3R5N3pUSGh1bENYYVZURHRUNHk3QU1VV0ZjVnJneEtNMkprR0Q5aG9vTnFzb2psYU8xMlR4QTM2emNIV3dCTzIzQll6UENtN1RjMmNIV0FEWnMzUGhnQVhQbXc4dWpKYjNmSExOdUVEQlRtcXl1eVV6eVNlVDMxS1BRM1FDejNGUXh3VmViWlBmUUd5K0FBPT0mbHQ7L2RpYWdyYW0mZ3Q7Jmx0Oy9teGZpbGUmZ3Q7Ij48ZGVmcy8+PGc+PHBhdGggZD0iTSAwIDAuNSBMIDMwIDM1LjUgTCAwIDcwLjUgWiIgZmlsbD0iI2U4NDU0OCIgc3Ryb2tlPSIjZTg0NTQ4IiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJhbGwiLz48L2c+PC9zdmc+"
                                            alt
                                            style="position: absolute;height:0.8rem;margin-left:-0.02rem; margin-top:-0.50rem;"
                                          />
                                          <!-- <hr  v-show="current_day == elem.date"  
                                            style="border-bottom:0.2rem solid #E84548; margin-top:0rem;border-top:0; margin-bottom:0" 
                                          />-->
                                          <div class="font-day" v-text="elem.day"></div>
                      
                                          <div>
                                            <transition-group
                                              name="staggered-fade"
                                              tag="div"
                                              v-bind:css="false"
                                              v-on:before-enter="beforeEnter"
                                              v-on:after-enter="afterEnter"
                                              v-on:enter="enter"
                                              v-on:leave="leave"
                                              v-on:before-leave="beforeLeave"
                                              v-on:after-leave="afterLeave"
                                              v-on:leave-cancelled="leaveCancelled"
                                              class="container-date-children"
                                              :id="[elem.more_than_two ? 'background-more-than-two' : '']"
                                            >
                                              <card
                                                v-for="(param_data,index_data) in elem.data"
                                                :index_column="index"
                                                class="div-body-date"
                                                :key="index_data+ 'card'"
                                                :param="param_data"
                                                :count="elem.count"
                                                :more_than_two="elem.more_than_two"
                                                :index="index_data"
                                                :color="param_data.color"
                                                :background_days="elem.background_days"
                                                :background="param_data.background"
                                                @getMenu="getMenu"
                                              ></card>
                                            </transition-group>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- week -->
                                    <div v-else key="2">
                                      <div
                                        class="container-dates-label-week"
                                        style="margin-top:0.7rem; padding-right:0.53rem"
                                      >
                                        <div style="width:100%"></div>
                                        <template v-for="elem in days_array_week">
                                          <div :key="elem.name">
                                            <div class="text-uppercase font-date-label-week">{{elem.name.substr(0,3)}}</div>
                                            <div class="text-center font-weight-bold">{{elem.day}}</div>
                                          </div>
                                        </template>
                                      </div>
                                      <div class="container-dates-week scroll-global">
                                        <div class="div-week-border">
                                          <div
                                            v-for=" hour  in hours_data"
                                            :key="hour.hour"
                                            class="div-children-week div-children-week-font"
                                          >
                                            <span class="text-left hour-format">{{hour.hour}} {{hour.format}}</span>
                                          </div>
                                        </div>
                                        <div
                                          v-for="(param,index) in days_array_week"
                                          :key="index"
                                          class="div-week-border"
                                          style="position:relative"
                                        >
                                          <div
                                            v-for="(paramh ,indexhs) in hours_data"
                                            :key="indexhs"
                                            class="div-children-week"
                                          ></div>
                      
                                          <img 
                                            v-show="current_day == param.date"
                                             src= "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMzFweCIgaGVpZ2h0PSI3MXB4IiB2aWV3Qm94PSItMC41IC0wLjUgMzEgNzEiIGNvbnRlbnQ9IiZsdDtteGZpbGUgaG9zdD0mcXVvdDt3d3cuZHJhdy5pbyZxdW90OyBtb2RpZmllZD0mcXVvdDsyMDIwLTAxLTIzVDIxOjIwOjE4Ljc5OFomcXVvdDsgYWdlbnQ9JnF1b3Q7TW96aWxsYS81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzc5LjAuMzk0NS4xMzAgU2FmYXJpLzUzNy4zNiZxdW90OyBldGFnPSZxdW90O29CSmptZDN0dzlPcHVQQTVsdVlYJnF1b3Q7IHZlcnNpb249JnF1b3Q7MTIuNS42JnF1b3Q7Jmd0OyZsdDtkaWFncmFtIGlkPSZxdW90O09hYmhOaWEwNGVvNDBuTVJzWXRVJnF1b3Q7IG5hbWU9JnF1b3Q7UMOhZ2luYS0xJnF1b3Q7Jmd0O2paTkxiNE13RElCL0RjZE5RS0R0cnFQdGR0aE9sVHFwdHdoY2tpMWdGTnhCKytzWGlpbXdoN1FUOXVkbmJPT0pwR2lmckt6VUsyWmd2TkRQV2src3ZUQU1oSGh3bjQ2Y2U3S0k0aDdrVm1mc05JS2R2Z0JEbitsSloxRFBIQW5Sa0s3bU1NV3loSlJtVEZxTHpkenRpR1pldFpJNS9BQzdWSnFmOUUxbnBIcTZpdjJSUDRQTzFWQTU4TmxTeU1HWlFhMWtoczBFaVkwbkVvdEl2VlMwQ1podWVNTmMrcmp0SDFadXJLYnowT3JRa1lXUy9wTmh2MSsvNHo0OWJCWEFvYmtFTDNWTWQ1ejJVNXJUTE8ya0Rsa3R5N3pUSGh1bENYYVZURHRUNHk3QU1VV0ZjVnJneEtNMkprR0Q5aG9vTnFzb2psYU8xMlR4QTM2emNIV3dCTzIzQll6UENtN1RjMmNIV0FEWnMzUGhnQVhQbXc4dWpKYjNmSExOdUVEQlRtcXl1eVV6eVNlVDMxS1BRM1FDejNGUXh3VmViWlBmUUd5K0FBPT0mbHQ7L2RpYWdyYW0mZ3Q7Jmx0Oy9teGZpbGUmZ3Q7Ij48ZGVmcy8+PGc+PHBhdGggZD0iTSAwIDAuNSBMIDMwIDM1LjUgTCAwIDcwLjUgWiIgZmlsbD0iI2U4NDU0OCIgc3Ryb2tlPSIjZTg0NTQ4IiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJhbGwiLz48L2c+PC9zdmc+"
                                            :style="{'position': 'absolute','height':'0.8rem','margin-left':'-0.07rem', 
                                                     'top': current_hour_mm + 'px'}"
                                          />
                      
                                          <hr
                                            v-show="current_day == param.date"
                                            :style="{'position': 'absolute','height':'0.1rem' ,
                                                     'top':( current_hour_mm - 11) + 'px', 'width': '100%', 'background': '#E84548'
                                                     }"
                                          />
                                          <template v-for="(param_data_px,index_px) in param.data">
                                            <card-week
                                              :key="index_px+'px'"
                                              :style="{ 'position':'absolute', 
                                                         'height':param_data_px.end_hours_number_px + 'px',
                                                         'top': param_data_px.star_date_px + 'px'}"
                                              class="div-body-date"
                                              :param="param_data_px"
                                              :index="index_px"
                                              :index_column="index"
                                              :color="param_data_px.color"
                                              :background_days="param.background_days"
                                              :background="param_data_px.background"
                                              state_end_date
                                              @getMenuWeek="getMenuWeek"
                                            />
                                          </template>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- end week  -->
                                  </transition>
                      
                                  <div v-show="state_menu" :style="getMenuPosition">
                                    <menu-item
                                        @saveSubMenu="saveSubMenu"
                                        @alertSubMenu="alertSubMenu"
                                        :user="user"
                                        @cancelMenu="state_menu = false"
                                        :data="menu_data"
                                        :state_right="state_right"
                                        :state_movil_menu="state_movil_menu"
                                        ref="menu-item"
                                        :icons_sub_menu="icons_sub_menu"
                                        class="z-101"
                                    />
                                  </div>
                      
                                  <div v-if="state_menu_week" :style="getMenuPositionWeek">
                                    
                                    <sub-menu 
                                      :icons_sub_menu="icons_sub_menu"
                                      :sub_menu_color="sub_menu_color"
                                      :sub_menu_background="sub_menu_background"
                                      :color_title ="color_title"
                                      @saveSubMenu="saveSubMenu"
                                      @alertSubMenu="alertSubMenu"
                                      :data="sub_menu_data"
                                      @setEstate="state_menu_week = false"
                                      :user="user"
                                      class="z-101"
                                    />
                                  </div>
                                </div>
                                <!-- List -->
                                <div v-else style="margin-top:0.7rem">
                                  <list
                                    :data="list_data"
                                    :user="user"
                                    :empty_messague="empty_messague"
                                    @saveSubMenu="saveSubMenu"
                                    @alertSubMenu="alertSubMenu"
                                    :icons_sub_menu="icons_sub_menu"
                                  />
                                </div>
                                <!-- end List -->
                              </transition>
                              <!-- end Body -->
                            </div>
                                        <button
                                         style="width: 10rem"
                                        type="button"
                                        class="btn btn-sm  div-border mt-4"
                                        @click="pushCalendar()"
                                         v-show="user == 'C'"
                                      >
                                         Agregar a Calendario
                                      </button>
                          </div>
                                   
                        </div>
                          `
    
const optionSubMenuList = {
  template: templateSubMenuList,
  props: {
    color_title: String, 
    icons_sub_menu: Object,
    sub_menu_background: String,
    user: String,
    data: [Object, Function, Array]
  },

  data() {
    return {
      input_save: ""
    };
  },
  created(){
    this.input_save = this.data.classroom
  },
  computed:{
        getStyleMovil(){
             if (window.innerWidth < 768) {
                return `overflow-y: auto;max-height: 30vh;padding-bottom:0.8rem`;
            }  else {
                return 'padding-bottom:1rem'
            }
        },
  },
  methods: {
    save() {
      this.data.input_text = this.input_save;
      this.$emit("saveSubMenu", this.data);
    },

    alert() {
      this.$emit("alertSubMenu", this.data);
    },

    setEstate() {
      this.$emit("setEstate");
    }
  }
}

const optionSubMenu = {
  template:templateSubMenu,
  props: {
    color_title: String, 
    icons_sub_menu: Object,
    sub_menu_background: String,
    user: String,
    data: [Object, Function, Array]
  },

  data() {
    return {
      input_save: ""
    };
  },
  created(){
    this.input_save = this.data.classroom
  },
  computed:{
        getStyleMovil(){
             if (window.innerWidth < 768) {
                return `overflow-y: auto;max-height: 30vh;padding-bottom:0.8rem`;
            }  else {
                return 'padding-bottom:1rem'
            }
        },
  },
  methods: {
    save() {
      this.data.input_text = this.input_save;
      this.$emit("saveSubMenu", this.data);
    },

    alert() {
      this.$emit("alertSubMenu", this.data);
    },

    setEstate() {
      this.$emit("setEstate");
    }
  }
}


const optionsCardWeek = {
  template:templateCardWeek,
 
  props: { 
    param: Object,
    index_column: Number,
    count: Number,
    more_than_two: Boolean,
    index: Number,
    color: String,
    background_days: Boolean,
    background: String,
    state_end_date: {
        default: false,
        type: Boolean
    }
},

data(){
    return {

    }
},

computed:{
  getStyle(){
    var vm = this 
    if (vm.background_days) {
        return `border: 1px solid  ${vm.color};
            border-radius: 0.12rem;
            border-left: 0.15rem solid ${vm.color};
            background: ${vm.background};
            position: absolute;
            left: ${vm.param.left}%;
            width: ${vm.param.width}%`
    } else {
         return `border: 1px solid  ${vm.color};
            border-radius: 0.12rem;
            border-left: 0.15rem solid ${vm.color};
            background: #FFF;
            position: absolute;
            left: ${vm.param.left}%;
            width: ${vm.param.width}%`
       
    }
}
},
methods: {
    getTitleDesktop(text){
        if (text != null && text != undefined && text != '') {
            if (text.length > 7) {
                return text.substr(0,6) + '...'
            } else {
                return text  
            }
            
        }
    },

    getTitleMovil(text){
        if (text != null && text != undefined && text != '') {
            if (text.length > 7) {
                return text.substr(0,6) + '...'
            } else {
                return text 
            }
            
        }
    },
    
    getMenu(event){ 
       var vm = this
           let param = {
               index: vm.index,
               index_column: vm.index_column,
               data: vm.param ,
               star_date_px: vm.param.star_date_px,
               event: event
           }
             
           vm.$emit('getMenuWeek', param )
       
       
    }
}
}

const optionsMenu = {
  template:templateMenu,
  props: {
    user: String,
    icons_sub_menu: Object,
    data: Array,
    state_movil_menu: Boolean,
    state_right: Boolean
},
 
data(){
    return {
        sub_data: Object,
        state_menu: false ,
        sub_menu_color: '',
        sub_menu_background: '',
        color_title: '',
        days_names: moment.weekdays(),
        months: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
    }
},

computed:{
   getStyleMovil(){
         if (window.innerWidth < 768) {
            return `overflow-y: auto;max-height: 30vh`;
        }  else {
            return ''
        }
    },
   getStyle(){
        var vm = this
         
         if (vm.state_movil_menu) {
             return `position:absolute;top:105%;width:100%; background: white; border-radius: 0.5rem ;`
         } else {
                    if (vm.state_right) {
                        return `position:absolute;top:0px ; right: 105% ;width:17rem;
                                            background: white; border-radius: 0.5rem ;`
                    } else {
                        return `position:absolute;top:0px ; left: 105% ;width:17rem;
                                            background: white; border-radius: 0.5rem ;`
                    }
         }

       
    },
},

methods:{
   saveSubMenu(data){
        this.$emit("saveSubMenu",  data);
   } ,

   alertSubMenu(data){
        this.$emit("alertSubMenu",  data);
   } ,
   setSubMenu(data){
       var vm = this
       let date = data.date
       let name = vm.days_names[moment(date, "YYYY-MM-DD").day()];
       name = name.charAt(0).toUpperCase() + name.slice(1);
       date = date.split('-')
       let date_name = `${name}, ${date[2]} de ${vm.months[parseInt(date[1]) -1]} de ${date[0]}`
       data.date_name = date_name 
       vm.sub_data = data
       vm.state_menu = true
       vm.sub_menu_color = data.color_title
       vm.sub_menu_background = data.background
       vm.color_title = data.color_title
   },

    cancelMenu(){
        this.$emit("cancelMenu" , false)
    }
}
}



const optionsCard =
{
  template: templateCard,
  props: {
    param: Object,
    index_column: Number,
    count: Number,
    more_than_two: Boolean,
    index: Number,
    color: String,
    background_days: Boolean,
    background: String,
    state_end_date: {
        default: false,
        type: Boolean
    }
},

computed:{

    getStyle(){
        var vm = this
         
        if (vm.background_days) {
            return `border: 1px solid  ${vm.color};
                border-radius: 0.12rem;
                border-left: 0.15rem solid ${vm.color};
                background: ${vm.background}`
        } else {
             return `border: 1px solid  ${vm.color};
                border-radius: 0.12rem;
                border-left: 0.15rem solid ${vm.color};
                background: #FFF`
           
        }
    }
},
data(){
    return {

    }
},
methods: {
    getTitleDesktop(text){
        if (text != null && text != undefined && text != '') {
            if (text.length > 7) {
                return text.substr(0,6) + '...'
            } else {
                return text  
            }
            
        }
    },

    getTitleMovil(text){
        if (text != null && text != undefined && text != '') {
            if (text.length > 7) {
                return text.substr(0,6) + '...'
            } else {
                return text 
            }
            
        }
    },
    
    getMenu(event){ 
       var vm = this
          
           let param = {
               index: vm.index,
               index_column: vm.index_column,
               event: event,
               more_than_two: vm.more_than_two
           }
           vm.$emit('getMenu', param)
       
       
    }
}    
}


const optionsList = {
  template: templateList,
  props: {
    user: String,
    data: Array,
    icons_sub_menu: Object,
    empty_messague: {
      type: String,
      default: ` <p class="text-center mt-4" >No se encontraro resultados</p>`
    }
  },
  
  computed:{
      getStyle(){
          return `width:17.5rem; padding:0.5rem;
                    position: absolute; top: 60px; right:22%;background:white;border-radius: 0.5rem;
                        filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`
      }
  },
  data() {
    return {
      list: false,
      menu_data: [],
      state_menu: false,
      state_movil_menu: false,
      index_global: Number,
      index_item: Number,
      color: '',
      background: '',
      color_title: '',
      days_names: moment.weekdays(),
      months: [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
      pageY: ''
    };
  },
  methods:{ 
        saveSubMenu(data){
            this.$emit('saveSubMenu', data)
        },

        alertSubMenu(data){
            this.$emit('alertSubMenu', data)
        },
        
        getMenu( e,events ) {    
           var vm = this    
           vm.pageY = e.pageY
           let date = events.date
           let name = vm.days_names[moment(date, "YYYY-MM-DD").day()];
           name = name.charAt(0).toUpperCase() + name.slice(1);
           date = date.split('-')
           let date_name = `${name}, ${date[2]} de ${vm.months[parseInt(date[1]) -1]} de ${date[0]}`
           events.date_name = date_name 
           vm.menu_data = events
           vm.state_menu = true,
           vm.background = events.background
           vm.color = events.color
           vm.color_title = events.color_title
        },
 
      setIcon(id){ 
          if ( document.getElementById(id).className == 'material-icons panel-heading' ) {
              document.getElementById(id).className =  'material-icons panel-heading panel-active'
          } else {
              document.getElementById(id).className =  'material-icons panel-heading'
          }
      }
  }
}


var optionsCalendar = {
template: templateCalendar,
props: {
  book_default: String,
  class_default: String,
  evaluation_default: String,
  icon_book_default: String,
  icon_class_default: String,
  icon_evaluation_default: String,
  user: String,
  empty_messague: String,
  view_body: Function,
  calendar_data: Array,
  icons_sub_menu: {
    type: Object,
    default: () => {
      return icons_sub_menu;
    }
  },
  icon_color_header: {
    type: String,
    default: "color-panel-select"
  },

  view_month: {
    type: Boolean,
    default: false
  },
  view_week: {
    type: Boolean,
    default: false
  },

  view_select: {
    type: Boolean,
    default: false
  },

  view_search: {
    type: Boolean,
    default: false
  },

  view_synchronize: {
    type: Boolean,
    default: false
  }
},
 
created() {
  this.getDataMonth();
},

watch: {
  search() {
    var vm = this;
    if (vm.type == "month") {
      vm.searchText(vm.search.toLowerCase());
    } else {
      vm.searchTextWeek(vm.search.toLowerCase());
    }
  }
},
data() {
  return {
    clientYMenu: "",
    index_column: "",
    state_menu: false,
    search: "",
    list: false,
    mode: "month",
    dataMonth: [],
    type: "month",
    month_select: "name",
    calendar_data_new: [],
    days_array: [],
    days_array_week: [],
    month_label: "",
    color_remaining_days: "#AAB7B8",
    color_days: "#212529",
    background_days: "#F0F9FE",
    background_before_days: "#FFFFFF",
    color_border_days: "#FFFFFF",
    // end props
    days_array_names: [
      { nameLg: "DOM", nameXs: "D" },
      { nameLg: "LUN", nameXs: "L" },
      { nameLg: "MAR", nameXs: "M" },
      { nameLg: "MIÉ", nameXs: "M" },
      { nameLg: "JUE", nameXs: "J" },
      { nameLg: "VIE", nameXs: "V" },
      { nameLg: "SÁB", nameXs: "S" }
    ],
    days_array_names_week: [],
    months: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Setiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    ],
    months_minimun: [
      "ENE",
      "FEB",
      "MAR",
      "ABR",
      "MAY",
      "JUN",
      "JUL",
      "AGO",
      "SET",
      "OCT",
      "NOV",
      "DIC"
    ],
    mmyy: "",
    ddmmyy: "",
    year: "",
    month: "",
    current_month: "",
    current_date: [],
    state_data: false,
    month_name: "",
    month_name_minimun: "",
    month_name_week: "",
    month_name_minimun_week: "",
    hours_data: [
      {
        hour: "",
        data: []
      },
      {
        hour: 1,
        data: [],
        format: "AM"
      },
      {
        hour: 2,
        data: [],
        format: "AM"
      },
      {
        hour: 3,
        data: [],
        format: "AM"
      },
      {
        hour: 4,
        data: [],
        format: "AM"
      },
      {
        hour: 5,
        data: [],
        format: "AM"
      },
      {
        hour: 6,
        data: [],
        format: "AM"
      },
      {
        hour: 7,
        data: [],
        format: "AM"
      },
      {
        hour: 8,
        data: [],
        format: "AM"
      },
      {
        hour: 9,
        data: [],
        format: "AM"
      },
      {
        hour: 10,
        data: [],
        format: "AM"
      },
      {
        hour: 11,
        data: [],
        format: "AM"
      },
      {
        hour: 12,
        data: [],
        format: "PM"
      },
      {
        hour: 13,
        data: [],
        format: "PM"
      },
      {
        hour: 14,
        data: [],
        format: "PM"
      },
      {
        hour: 15,
        data: [],
        format: "PM"
      },
      {
        hour: 16,
        data: [],
        format: "PM"
      },
      {
        hour: 17,
        data: [],
        format: "PM"
      },
      {
        hour: 18,
        data: [],
        format: "PM"
      },
      {
        hour: 19,
        data: [],
        format: "PM"
      },
      {
        hour: 20,
        data: [],
        format: "PM"
      },
      {
        hour: 21,
        data: [],
        format: "PM"
      },
      {
        hour: 22,
        data: [],
        format: "PM"
      },
      {
        hour: 23,
        data: [],
        format: "PM"
      }
    ],
    menu_data: [],
    sub_menu_data: {},
    list_data: [],
    state_movil_menu: false,
    state_right: false,
    index_column_index: 0,
    state_menu_week: false,
    sub_menu_color: "",
    sub_menu_background: "",
    color_title: ""
  };
},
computed: {
  getMenuPosition() {
    var vm = this;
    let top = Number;
    let left = Number;
    let top_number = Number
    let right = false;
    let index_column = vm.index_column.index_column + 1;
    let index = vm.index_column.index;
    let left_number = 0;
    if (index_column < 8) {
      //primera fila
      if (index_column == 6 || index_column == 7) {
        //columnas penultima y ultima
        top = -70;
        left = (7.52 - index_column) * 14.28;
        right = true;
      } else {
        top = -70;
        left = index_column * 14.28;
      }
      top_number = 1
    } else {
      //resto de filas
      top_number = Math.ceil(parseInt(index_column) / 7) - 1;
      left_number = parseInt(index_column) % 7;
      left_number = left_number == 0 ? 7 : left_number;
      if (left_number == 6 || left_number == 7) {
        //columnas penultima y ultima
        right = true;
        top = top_number * 70;
        left = (7.52 - left_number) * 14.28;
      } else {
        top = top_number * 70;
        left = left_number * 14.28;
      }
    }

    vm.state_right = right;
    if ( left_number == 5 ) {
      vm.state_right = true;
    }

    vm.state_movil_menu = window.innerWidth < 768 ? true : false;
    top = top -120
    if ( window.innerWidth < 768 ) {
      // movil
      return `width:96%;position:absolute; top:5%;left:2%; right:2%;background:white;border-radius: 0.5rem;
                    filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
    } else if ( right ) {
      //desktop
      return `width:17.5rem;position:absolute;top:${top}px;right:${left}%;background:white;border-radius: 0.5rem;
                     filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
    } else {
      return `width:17.5rem;position:absolute;top:${top}px;left:${left}%;background:white;border-radius: 0.5rem;
                     filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
    }
  },

  getMenuPositionWeek() {
    var vm = this;
    let top = Number;
    let left = Number;
    let right = false;
    let index_column = vm.index_column.index_column + 1;
    let index = vm.index_column.index;
    let top_number = 0
    let left_number = 0;
    if (index_column < 8) {
      left_number = index_column;
      top_number = 1
    } else {
      //resto de filas
      top_number = Math.ceil(parseInt(index_column) / 7) ;
      left_number = parseInt(index_column) % 7;
      left_number = left_number == 0 ? 7 : left_number;
  
    }

    vm.state_right = right;
    if (vm.type == "week") {
      if (left_number >= 5) {
        vm.state_right = true;
        right = true;
      }
       top = vm.user == "D" || vm.user == "A" ? 50 : (vm.clientYMenu - 240);
    } else {
      if (left_number >= 6 || vm.state_menu) {
        vm.state_right = true;
        right = true;
      }

      top = vm.user == "D" || vm.user == "A" ? 50 : (top_number*90 - 150);
    }
    top =20
    vm.state_movil_menu = window.innerWidth < 768 ? true : false;
    if (vm.state_movil_menu) {
      // movil
      return `width:96%;position:absolute; top:5%;left:2%; right:2%;background:white;border-radius: 0.5rem;
                    filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
    } else if (right) {
      //desktop
      if (vm.type == "week") {
        left = (7.8 - left_number) * 13.7 + 4.1;
      } else {
        left = (8 - left_number) * 14.28;
        left = index == 1 ? left + 1 : left;
      }

      return `width:17.5rem;position:absolute;top:${top}px;right:${left}%;background:white;border-radius: 0.5rem;
                     filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
    } else {
      if (vm.type == "week") {
        left = left_number * 13.7 + 4.8;
      } else {
        left = left_number * 14.28;
        left = index == 0 ? left - 6.6 : left;
      }

      return `width:17.5rem;position:absolute;top:${top}px;left:${left}%;background:white;border-radius: 0.5rem;
                     filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
    }
  },

  current_day() {
    var vm = this;
    return moment().format("DD-MM-YYYY");
  },

  current_hour_mm() {
    var vm = this;
    let date = moment().format("H:mm");
    date = date.split(":");
    return parseInt(date[0]) * 30 + (parseInt(date[1]) / 60) * 30;
  }
},

methods: {
  pushCalendar(){
    this.$emit("pushcalendar");
  },
  getMenuBtn() {
    this.$emit("getmenubtn");
  },
  onPanHorizontal(e) {
    // console.log('onPanHorizontal',e)
  },

  onPanStart(e) {
    // console.log('onPanStart',e)
  },

  onPanEnd(e) {
    var vm = this;
    if (window.innerWidth < 768) {
      if (e.deltaX < -200 || e.deltaX > 200) {
        if (e.additionalEvent == "panleft") {
          vm.setMonth(true);
        } else {
          vm.setMonth(false);
        }
      }
    }
  },

  onPress(e) {
    // console.log('onPress',e)
  },

  onPressup(e) {
    // console.log('onPressup',e)
  },
  onTap(e) {
    // console.log('onTap',e)
  },

  getMenu(param) {
    let index = param.index;
    let index_column = param.index_column;
    let more_than_two = param.more_than_two;
    var vm = this;
    vm.clientYMenu = param.event.clientY;

    if (vm.index_column_index != index_column + "-" + index) {
      vm.$refs["menu-item"].state_menu = false;
    }
    vm.state_menu_week = false;
    vm.state_menu = false;
    vm.index_column_index = index_column + "-" + index;
    let data =
      vm.days_array[index_column].data[index].ids_data == undefined
        ? vm.days_array[index_column].data
        : vm.days_array[index_column].data[index].ids_data;

    if (index == 0 || !more_than_two) {
      data = data[index];
      vm.index_column = param;
      vm.state_menu_week = true;
      vm.sub_menu_color = data.color;
      vm.sub_menu_background = data.background;
      vm.color_title = data.color_title
      let date = data.date.split('-')
      let name = data.name
       
      let date_name = `${name.charAt(0).toUpperCase() + name.slice(1)}, ${date[2]} de ${
            vm.months[parseInt(date[1]) - 1]
          } de ${date[0]}`;
      data.date_name = date_name;  
      console.log(data)
      vm.sub_menu_data = data;
    } else {
      if (index == 0) {
        data = [vm.days_array[index_column].data[0]];
      }

      if (data != undefined) {
        let ids = [];
        let menu_data = [];
        vm.state_menu = true;
        let days_names = moment.weekdays();
        vm.index_column = param;
        for (let index = 0; index < data.length; index++) {
          if(data[index].id == null){
            ids.push(data[index].sub_title);
          } else {
            ids.push(parseInt(data[index].id));
          }
          
        }

        ids = ids.filter(function(value, index, self) {
          //eliminar duplicados
          return self.indexOf(value) === index;
        });
        
        ids.forEach((element, index) => {
          menu_data.push({
            sub_title: "",
            color: "",
            data: []
          });
          data.forEach(element_data => {
            if (parseInt(element_data.id) == element && typeof(element)) {
              if (menu_data[index].data.length == 0) {
                menu_data[index].sub_title = element_data.sub_title;
                menu_data[index].color = element_data.color;
                menu_data[index].color_title = element_data.color_title;
                menu_data[index].data = [element_data];
              } else {
                menu_data[index].data.push(element_data);
              }
            } else if(element_data.sub_title == element){
               if (menu_data[index].data.length == 0) {
                menu_data[index].sub_title = element_data.sub_title;
                menu_data[index].color = element_data.color;
                menu_data[index].color_title = element_data.color_title;
                menu_data[index].data = [element_data];
              } else {
                menu_data[index].data.push(element_data);
              }
            }
          });
        });

        let name =
          vm.days_array[index_column].name.charAt(0).toUpperCase() +
          vm.days_array[index_column].name.slice(1);
        let date = vm.days_array[index_column].date.split("-");

        let date_name = `${name}, ${date[0]} de ${
          vm.months[parseInt(date[1]) - 1]
        } de ${date[2]}`;
        menu_data.date_name = date_name;
        console.log(menu_data)
        vm.menu_data = menu_data;
      }
    }
  },

  getMenuWeek(param) {
    let index = param.index;
    var vm = this;
    vm.clientYMenu = param.event.pageY;
    vm.index_column = param;
    let data = param.data;
    vm.state_menu_week = true;

    vm.sub_menu_color = data.color_title;
    vm.sub_menu_background = data.background;
    vm.color_title = data.color_title;
    let date = data.date.split('-')
    let name = data.name
    let date_name = `${name.charAt(0).toUpperCase() + name.slice(1)}, ${date[2]} de ${
          vm.months[parseInt(date[1]) - 1]
        } de ${date[0]}`;
    data.date_name = date_name;

    vm.sub_menu_data = data;
  },

  beforeLeave(el, done) {
    el.style.opacity = 0;
  },

  afterLeave(el, done) {
    el.style.opacity = 0.6;
  },

  leaveCancelled(el, done) {},

  afterEnter(el, done) {},

  beforeEnter(el, done) {
    el.style.opacity = 0.3;
  },

  enter(el, done) {
    setTimeout(() => {
      el.style.opacity = 1;
    }, 100);
  },

  leave(el, done) {
    // console.log(el.dataset )
    //  setTimeout(() => {
    //    el.style.opacity = 0
    // }, 300);
  },

  setType(type) {
    var vm = this;
    vm.mode = type;
    if (type == "list") {
      vm.setDataList(vm.mmyy);
    } else {
      vm.onChangeType(vm.type);
    }
  },

  setDataList(mmyy) {
    var vm = this;
    let ids = [];
    let programs = [];
    let programs_courses = [];
    let courses = [];
    let list_data = [];
    let data_list = [];
    let star_date = "01-" + mmyy;
    star_date = moment(star_date, "DD-MM-YYYY").format("YYYY-MM-DD");
    let end_date = moment(star_date, "YYYY-MM-DD")
      .add(1, "month")
      .format("YYYY-MM-DD");
    end_date = moment(end_date, "YYYY-MM-DD")
      .add(-1, "day")
      .format("YYYY-MM-DD");

    for (let index = 0; index < vm.calendar_data_new.length; index++) {
      let date = vm.calendar_data_new[index].date;
      let after_date = moment(date).isSameOrAfter(star_date);
      let before_date = moment(date).isSameOrBefore(end_date); 
      if (after_date && before_date) {
        let state_exist_program = false;
        if (vm.calendar_data_new[index].id != null) {
          //programas
          for (
            let index_prog = 0;
            index_prog < programs.length;
            index_prog++
          ) {
            if (programs[index_prog].id == vm.calendar_data_new[index].id) {
              state_exist_program = true;
              break;
            }
          }

          if (!state_exist_program) {
            programs.push({
              _id: vm.calendar_data_new[index]._id,
              program: vm.calendar_data_new[index].program,
              title: vm.calendar_data_new[index].title,
              id: vm.calendar_data_new[index].id,
              place: vm.calendar_data_new[index].place,
              academic_coordination_person: vm.calendar_data_new[index].academic_coordination_person,
              type: "P",
              type_text: vm.calendar_data_new[index].type,
              program_dstart: vm.calendar_data_new[index].program_dstart,
              program_dend: vm.calendar_data_new[index].program_dend,
              courses: []
            });
          }

           
        } else {
          // cursos
          let state_exist_program_course = false;
          for (
            let index_prog = 0;
            index_prog < programs_courses.length;
            index_prog++
          ) {
            if (
              programs_courses[index_prog].sub_title ==
                vm.calendar_data_new[index].sub_title &&
              programs_courses[index_prog].id == null
            ) {
              state_exist_program_course = true;
              break;
            }
          }

          if (!state_exist_program_course) {
            programs_courses.push({
              type: "C",
              modality: vm.calendar_data_new[index].modality,
              id: vm.calendar_data_new[index].id,
              sub_title: vm.calendar_data_new[index].sub_title,
              title: vm.calendar_data_new[index].title,
              text: vm.calendar_data_new[index].text,
              color: vm.calendar_data_new[index].color,
              background: vm.calendar_data_new[index].background,
              color_title:vm.calendar_data_new[index].color_title,
              type_text: vm.calendar_data_new[index].type,
              course_dstart: vm.calendar_data_new[index].course_dstart,
              course_dend: vm.calendar_data_new[index].course_dend,
              sessions: vm.calendar_data_new[index].sessions,
              events: []
            });

          }
        }
       
        let state_exist_cursos = false;
        for (
          let index_courses = 0;
          index_courses < courses.length;
          index_courses++
        ) {
          if (
            courses[index_courses].sub_title ==
            vm.calendar_data_new[index].sub_title
          ) {
            state_exist_cursos = true;
            break;
          }
        }

        if (!state_exist_cursos) {
          //todos los cursos
          courses.push({
            id: vm.calendar_data_new[index].id,
            sub_title: vm.calendar_data_new[index].sub_title,
            title: vm.calendar_data_new[index].title,
            text: vm.calendar_data_new[index].text,
            modality: vm.calendar_data_new[index].modality,
            color: vm.calendar_data_new[index].color,
            background: vm.calendar_data_new[index].background,
            course_dstart: vm.calendar_data_new[index].course_dstart,
            course_dend: vm.calendar_data_new[index].course_dend,
            sessions: vm.calendar_data_new[index].sessions
          });
        }

        let date_name = vm.calendar_data_new[index].date.split("-");
        data_list.push({
          _id: vm.calendar_data_new[index]._id,
          id: vm.calendar_data_new[index].id,
          title: vm.calendar_data_new[index].title,
          sub_title: vm.calendar_data_new[index].sub_title,
          star_hours: vm.calendar_data_new[index].star_hours,
          star_hours_number: vm.calendar_data_new[index].star_hours_number,
          end_hours: vm.calendar_data_new[index].end_hours,
          end_hours_number: vm.calendar_data_new[index].end_hours_number,
          name: vm.calendar_data_new[index].name,
          dateY: date_name[0],
          dateM: date_name[1],
          dateD: date_name[2],
          date: vm.calendar_data_new[index].date,
          name_month: vm.months[parseInt(date_name[1]) - 1],
          description: vm.calendar_data_new[index].description,
          text: vm.calendar_data_new[index].text,
          modality: vm.calendar_data_new[index].modality,
          academic_coordination_person:
            vm.calendar_data_new[index].academic_coordination_person,
          academic_coordination_phone:
            vm.calendar_data_new[index].academic_coordination_phone,
          academic_coordination_email:
            vm.calendar_data_new[index].academic_coordination_email,
          help_desk_person: vm.calendar_data_new[index].help_desk_person,
          help_desk_phone: vm.calendar_data_new[index].help_desk_phone,
          place: vm.calendar_data_new[index].place,
          color: vm.calendar_data_new[index].color,
          background: vm.calendar_data_new[index].background,
          color_title: vm.calendar_data_new[index].color_title,
          classroom: vm.calendar_data_new[index].classroom,
          eventid: vm.calendar_data_new[index].eventid,
          docente: vm.calendar_data_new[index].docente,
          platform: vm.calendar_data_new[index].platform
        });
      }
    }

    programs.forEach(element => {
      courses.forEach((element_courses, indexC) => {
        if (element.id == element_courses.id) {
          let data = [];
          for (let index = 0; index < data_list.length; index++) {
            
            if (
              element_courses.sub_title == data_list[index].sub_title  
            ) {
              data.push(data_list[index]);
            }
          }
         
          element_courses.events = data;
          element.courses.push(element_courses);
        }
      });
    });

    programs_courses.forEach((element_courses, indexC) => {
      let data = [];
      for (let index = 0; index < data_list.length; index++) {
        if (
          element_courses.sub_title == data_list[index].sub_title &&
          data_list[index].id == null
        ) {
          data.push(data_list[index]);
        }
      }
   
      element_courses.events = data;
    });

    // for (let index = 0; index < programs.length; index++) {
    //   programs[index].star_date = programs[index].courses[0].star_date;
    //   programs[index].end_date =
    //   programs[index].courses[programs[index].courses.length - 1].end_date;
    // }

    programs = programs.concat(programs_courses);
    vm.list_data = programs;
  },

  async onChangeType(type) {
    var vm = this;
    vm.state_menu = false;
    vm.type = type;
    vm.state_menu_week = false;
    if (type != "month" && vm.days_array_week.length == 0) {
      vm.ddmmyy = "01-" + vm.mmyy;
      
      vm.weekCompare(
        await vm.getdaysArrayByWeek(vm.ddmmyy),
        vm.calendar_data_new
      );
    } else {
      vm.compare(await vm.getdaysArrayByMonth(vm.mmyy), vm.calendar_data_new);
    }
  },

  getdaysArrayByMonth(mmyy) {
    var vm = this;
    moment.locale("es");
    let days = moment(mmyy, "MM-YYYY").daysInMonth(); //cantidad de días del mes
    let date_array = []; //array de mes a llenar
    let days_names = moment.weekdays();
    let current_date = moment().format("YYYY-MM-DD");
    for (let index = 0; index < days; index++) {
      // fechas de mes actual
      let index_date =
        index < 9 ? "0" + String(index + 1) : String(index + 1);
      let date = index_date + "-" + mmyy;
      let date_original = moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");

      let name = days_names[moment(date, "DD-MM-YYYY").day()];

      date_array.push({
        date: date,
        date_original: date_original,
        name: name,
        day: String(index + 1),
        background_days: true,
        data: [],
        count: 0,
        more_than_two: false
      });
    }

    if (date_array[0].name != "domingo") {
      // agregar días faltas hacía atras
      date_array[0].day = vm.month_name_minimun + " " + date_array[0].day;
      for (let index = 0; index < 11; index++) {
        let date = moment("1-" + mmyy, "DD-MM-YYYY")
          .add(-1 * (index + 1), "day")
          .format("DD-MM-YYYY");
        let name = days_names[moment(date, "DD-MM-YYYY").day()];
        let date_original = moment(date, "DD-MM-YYYY")
          .format("YYYY-MM-DD");

        date_array.unshift({
          date: date,
          date_original: date_original,
          name: name,
          day: parseInt(date.substr(0, 2)),
          background_days: true,
          data: [],
          count: 0,
          more_than_two: false
        });
        if (name == "domingo") {
          break;
        }
      }
    }

    let count_after = 42 - date_array.length;
    for (let index = 0; index < count_after; index++) {
      // agregar días faltantes hacia adelante
      let date = moment(date_array[date_array.length - 1].date, "DD-MM-YYYY")
        .add(1, "day")
        .format("DD-MM-YYYY");

      let name = days_names[moment(date, "DD-MM-YYYY").day()];
      date_array.push({
        date: date,
        date_original: moment(date, "DD-MM-YYYY").format("YYYY-MM-DD"),
        name: name,
        day: parseInt(date.substr(0, 2)),
        background_days: true,
        data: [],
        count: 0,
        more_than_two: false
      });
    }

    date_array.forEach(element => {
      //color antes de fecha actual
      let state = moment(element.date_original, "YYYY-MM-DD")
        .isSameOrAfter(current_date, "YYYY-MM-DD");

      element.background_days = state;
    });
    vm.calendar_dataMonth = date_array;
    return new Promise(resolve => {
      resolve(date_array);
    });
  },

  getdaysArrayByWeek(ddmmyy) {
    var vm = this;
    let days_names = moment.weekdays();
    let date = ddmmyy;
    let name = days_names[moment(date, "DD-MM-YYYY").day()];
    let date_array_week = [];

    let current_date = moment().format("YYYY-MM-DD");

    date_array_week.push({
      date: date,
      day: moment(date, "DD-MM-YYYY").format("DD"),
      date_original: moment(date, "DD-MM-YYYY").format("YYYY-MM-DD"),
      name: name,
      background_days: true,
      data: [],
      count: 0,
      more_than_two: false
    });

    if (date_array_week[0].name != "domingo") {
      // agregar días faltas hacía atras

      for (let index = 0; index < 7; index++) {
        let before_date = moment(date, "DD-MM-YYYY")
          .add(-1 * (index + 1), "day")
          .format("DD-MM-YYYY");

        let name = days_names[moment(before_date, "DD-MM-YYYY").day()];
        let date_original = moment(before_date, "DD-MM-YYYY")
          .format("YYYY-MM-DD");

        date_array_week.unshift({
          date: before_date,
          day: moment(before_date, "DD-MM-YYYY").format("DD"),
          date_original: date_original,
          name: name,
          background_days: true,
          data: [],
          count: 0,
          more_than_two: false
        });
        if (name == "domingo") {
          break;
        }
      }
    }

    let count_after = 7 - date_array_week.length;
    for (let index = 0; index < count_after; index++) {
      // agregar días faltantes hacia adelante
      let after_date = moment(
          date_array_week[date_array_week.length - 1].date,
          "DD-MM-YYYY"
        )
        .add(1, "day")
        .format("DD-MM-YYYY");

      let name = days_names[moment(after_date, "DD-MM-YYYY").day()];
      date_array_week.push({
        date: after_date,
        day: moment(after_date, "DD-MM-YYYY").format("DD"),
        date_original: moment(after_date, "DD-MM-YYYY")
          .format("YYYY-MM-DD"),
        name: name,
        background_days: true,
        count: 0,
        more_than_two: false
      });
    }

    date_array_week.forEach(element => {
      //color antes de fecha actual
      let state = moment(element.date_original, "YYYY-MM-DD")
        .isSameOrAfter(current_date, "YYYY-MM-DD");

      element.background_days = state;
    });

    vm.days_array_week = date_array_week;

    return new Promise(resolve => {
      resolve(date_array_week);
    });
    //set horas restantes
  },

  async onChangeMonth(mm) {
    var vm = this;
    let a = vm.mmyy.split("-")[1] + "-" + mm;
    if (
      moment(a, "YYYY-MM-DD")
        .isSame(moment().format("YYYY-MM"), "YYYY-MM")
    ) {
      vm.month_select = "name";
    }
    moment.locale("es");
    let yy = vm.mmyy.split("-")[1];
    let param = vm.mmyy.split("-");

    vm.month = mm; //mes y año actual
    vm.month_name = vm.months[parseInt(mm) - 1];
    vm.month_name_minimun = vm.months_minimun[parseInt(mm) - 1];
    vm.month_label = vm.month_name + " de " + yy;
    vm.mmyy = mm + "-" + yy;
    await vm.getdaysArrayByMonth(vm.mmyy);
    vm.searchText(vm.search);
  },

  async setDay() {
    var vm = this;
    moment.locale("es");
    vm.mmyy = String(moment().format("MM-YYYY"));
    let param = vm.mmyy.split("-");
    vm.month_name = vm.months[parseInt(param[0]) - 1];
    vm.month_name_minimun = vm.months_minimun[parseInt(param[0]) - 1];
    vm.month_label = vm.month_name + " de " + param[1];
    if (vm.type == "month") {
      vm.month_select = "name";
      vm.month = moment().format("MM"); //mes y año actual
      await vm.getdaysArrayByMonth(vm.mmyy);
      vm.searchText(vm.search);
    } else {
      vm.month_select = "name";
      vm.ddmmyy =  String(moment().format("DD-MM-YYYY"));
      await vm.getdaysArrayByWeek(vm.ddmmyy);
      vm.searchTextWeek(vm.search);
    }
  },

  async getDataMonth() {
    //Formateo de data
    var vm = this;
    moment.locale("es");
    //vm.months = vm.$moment.months(); //nombres de meses del año

    vm.mmyy =   ( vm.mmyy ) ? vm.mmyy :  String(moment().format("MM-YYYY"));
    vm.ddmmyy = (vm.ddmmyy) ? vm.ddmmyy : String(moment().format("DD-MM-YYYY"));
    let param = vm.mmyy.split("-");

    vm.month = moment().format("MM"); //mes actual
    vm.current_month = moment().format("MM"); //mes  actual no mutable
    vm.month_name = vm.months[parseInt(param[0]) - 1];
    vm.month_name_minimun = vm.months_minimun[parseInt(param[0]) - 1];
    vm.month_label = vm.month_name + " de " + param[1];
    let data_new = [];

    let days_names = moment.weekdays();

    for (let index = 0; index < vm.calendar_data.length; index++) {
      let data = vm.calendar_data[index];
      for (
        let indexEvent = 0;
        indexEvent < vm.calendar_data[index].events.length;
        indexEvent++
      ) {
        let data_event = vm.calendar_data[index].events[indexEvent];
        let date_name = moment
          .unix(data_event.star_date)
          .format("YYYY-MM-DD");
        let name = days_names[moment(date_name, "YYYY-MM-DD").day()];
        name = name.charAt(0).toUpperCase() + name.slice(1)
        
        let start_new_format_date = moment
          .unix(data_event.star_date)
          .format("YYYY-MM-DD, h:mm a , H:mm")
          .split(",");

        let end_new_format_date = moment
          .unix(data_event.end_date)
          .format("YYYY-MM-DD, h:mm a , H:mm")
          .split(",");
        let state = moment(end_new_format_date[0], "YYYY-MM-DD")
          .isSameOrAfter(start_new_format_date[0], "YYYY-MM-DD");

        // Segun el tipo
          switch ( data_event.type ) {
             case "C":
              data_event.title = `<i style="font-size:11px" class="material-icons">${ vm.icon_class_default }</i> ${vm.class_default}`
              break;
             case "T":
              data_event.title =  `<i style="font-size:11px" class="material-icons">${ vm.icon_book_default }</i> ${vm.book_default}`
              break;
             case "E":
              data_event.title =  `<i style="font-size:11px" class="material-icons">${ vm.icon_evaluation_default }</i> ${vm.evaluation_default}` 
             break;
             default:
              data_event.title =   `<i style="font-size:11px" class="material-icons">${ vm.data_event.icon_title }</i> ${vm.data_event.title}`
             break;
          }
          
        // fin segun tipo

        if( data.color == '' || data.color == null || data.color ==  undefined ){
          let position_color = Math.floor(Math.random() * 9);
          data.color = colors[position_color].color
          data.color_title = colors[position_color].color_title
          data.background = colors[position_color].background
        }

        if (state) {
          // La fecha end, debe ser mayor o igual a la inicial para ser considerada
          let difference = moment(end_new_format_date[0])
            .diff(moment(start_new_format_date[0]), "days");

          if (difference > 0) {
            // Separación de fechas, si abarca más de un día
            difference = difference + 1;
            let date_new = ''
            for (
              let index_difference = 0;
              index_difference < difference;
              index_difference++
            ) {
              //Diviendo en bloques
              if (index_difference == 0) {
                data_new.push({
                  name: name,
                  classroom: data_event.classroom,
                  eventid: data_event.id,
                  responsable_id: data.responsable_id,
                  program_dstart:  moment.unix( data.program_dstart ).format("DD/MM/YYYY"), 
                  program_dend:  moment.unix( data.program_dend ).format("DD/MM/YYYY"),
                  course_dstart: moment.unix( data.course_dstart ).format("DD/MM/YYYY"),
                  course_dend: moment.unix( data.course_dend ).format("DD/MM/YYYY"),
                  modality: data_event.modality,
                  docente: data.docente,
                  type: data.type,
                  academic_coordination_person: data.academic_coordination_person,
                  academic_coordination_phone: data.academic_coordination_phone,
                  academic_coordination_email: data.academic_coordination_email,
                  help_desk_person: data.help_desk_person,
                  help_desk_phone: data.help_desk_phone,
                  platform: data_event.platform,
                  program: data.title,
                  sub_title: data.sub_title,
                  sessions: data.sessions,
                  id: data.id,
                  _id: data._id,
                  color: data.color,
                  background: data.background,
                  description: data_event.description,
                  place: data_event.place,
                  text: data_event.text,
                  title: data_event.title,
                  date: start_new_format_date[0],
                  star_hours: start_new_format_date[1],
                  star_hours_number: start_new_format_date[2],
                  end_hours: "11:59 pm",
                  end_hours_number: "23:59",
                   color_title: data.color_title
                });
              } else if (index_difference == difference - 1) {
                date_new =  moment(data_new[data_new.length - 1].date, "YYYY-MM-DD")
                    .add(1, "day")
                    .format("YYYY-MM-DD")
                name =  days_names[moment(date_new, "YYYY-MM-DD").day()]
                name =  name.charAt(0).toUpperCase() + name.slice(1)
                data_new.push({
                  name: name,
                  classroom: data_event.classroom,
                  eventid: data_event.id,
                  responsable_id: data.responsable_id,
                  program_dstart:  moment.unix( data.program_dstart ).format("DD/MM/YYYY"), 
                  program_dend:  moment.unix( data.program_dend ).format("DD/MM/YYYY"),
                  course_dstart: moment.unix( data.course_dstart ).format("DD/MM/YYYY"),
                  course_dend: moment.unix( data.course_dend ).format("DD/MM/YYYY"),
                  type: data.type,
                  modality: data_event.modality,
                  docente: data.docente,
                  academic_coordination_person: data.academic_coordination_person,
                  academic_coordination_phone: data.academic_coordination_phone,
                  academic_coordination_email: data.academic_coordination_email,
                  help_desk_person: data.help_desk_person,
                  help_desk_phone: data.help_desk_phone,
                  program: data.title,
                  platform: data_event.platform,
                  sub_title: data.sub_title,
                  sessions: data.sessions,  
                  id: data.id,
                  _id: data._id,
                  color: data.color,
                  background: data.background,
                  description: data_event.description,
                  place: data_event.place,
                  text: data_event.text,
                  title: data_event.title,
                  date: date_new,
                  star_hours: "0:00 am",
                  star_hours_number: "0:00",
                  end_hours: end_new_format_date[1],
                  end_hours_number: end_new_format_date[2],
                  color_title: data.color_title
                });
              } else {
                date_new = moment(data_new[data_new.length - 1].date, "YYYY-MM-DD")
                             .add(1, "day")
                             .format("YYYY-MM-DD")
                name = days_names[moment(date_new, "YYYY-MM-DD").day()],
                name =  name.charAt(0).toUpperCase() + name.slice(1)
                data_new.push({
                  name: name,
                  classroom: data_event.classroom,
                  eventid: data_event.id,
                  responsable_id: data.responsable_id,
                  program_dstart:  moment.unix( data.program_dstart ).format("DD/MM/YYYY"), 
                  program_dend:  moment.unix( data.program_dend ).format("DD/MM/YYYY"),
                  course_dstart: moment.unix( data.course_dstart ).format("DD/MM/YYYY"),
                  course_dend: moment.unix( data.course_dend ).format("DD/MM/YYYY"),
                  type: data.type,
                  modality: data_event.modality,
                  docente: data.docente,
                  academic_coordination_person: data.academic_coordination_person,
                  academic_coordination_phone: data.academic_coordination_phone,
                  academic_coordination_email: data.academic_coordination_email,
                  help_desk_person: data.help_desk_person,
                  help_desk_phone: data.help_desk_phone,
                  program: data.title,
                  platform: data_event.platform,
                  sub_title: data.sub_title,
                  sessions: data.sessions,  
                  id: data.id,
                  _id: data._id,
                  color: data.color,
                  background: data.background,
                  description: data_event.description,
                  place: data_event.place,
                  text: data_event.text,
                  title: data_event.title,
                  date: date_new,
                  star_hours: "0:00 am",
                  star_hours_number: "0:00",
                  end_hours: "23:59 pm",
                  end_hours_number: "23:59",
                   color_title: data.color_title
                });
              }
            }
          } else {
            data_new.push({
              name: name,
              classroom: data_event.classroom,
              eventid: data_event.id,
              responsable_id: data.responsable_id,
              program_dstart:  moment.unix( data.program_dstart ).format("DD/MM/YYYY"), 
              program_dend:  moment.unix( data.program_dend ).format("DD/MM/YYYY"),
              course_dstart: moment.unix( data.course_dstart ).format("DD/MM/YYYY"),
              course_dend: moment.unix( data.course_dend ).format("DD/MM/YYYY"),
              type: data.type,
              modality: data_event.modality,
              docente: data.docente,
              academic_coordination_person: data.academic_coordination_person,
              academic_coordination_phone: data.academic_coordination_phone,
              academic_coordination_email: data.academic_coordination_email,
              help_desk_person: data.help_desk_person,
              help_desk_phone: data.help_desk_phone,
              program: data.title,
              platform: data_event.platform,
              sub_title: data.sub_title,
              sessions: data.sessions,  
              id: data.id,
              _id: data._id,
              color: data.color,
              background: data.background,
              description: data_event.description,
              place: data_event.place,
              text: data_event.text,
              title: data_event.title,
              date: start_new_format_date[0],
              star_hours: start_new_format_date[1],
              star_hours_number: start_new_format_date[2],
              end_hours: end_new_format_date[1],
              end_hours_number: end_new_format_date[2],
              color_title: data.color_title
            });
          }
        }
      }
    }

    // Ordenar data por fecha
    data_new = data_new.sort(function(left, right) {
      return moment.utc(left.date).diff(moment.utc(right.date));
    });
    // end ordernar data

    vm.calendar_data_new = [...data_new];
    if ( vm.type == 'month' ) {
       vm.compare(await vm.getdaysArrayByMonth(vm.mmyy), data_new);
    } else {
        vm.weekCompare(await vm.getdaysArrayByWeek(vm.ddmmyy), data_new);
      
    }
   
  },

  async setMonth(state) {
    var vm = this;
    moment.locale("es");
    vm.state_menu = false;
    vm.state_menu_week = false;
    let param = [];
    let data_emit = [];
    if (vm.mode == "month") {
      if (vm.type == "month") {
        if (state) {
          vm.mmyy = moment("1-" + vm.mmyy, "DD-MM-YYYY")
            .add(1, "month")
            .format("MM-YYYY");
          param = vm.mmyy.split("-");
        } else {
          vm.mmyy = moment("1-" + vm.mmyy, "DD-MM-YYYY")
            .add(-1, "month")
            .format("MM-YYYY");
          param = vm.mmyy.split("-");
        }
        vm.month_name = vm.months[parseInt(param[0]) - 1];
        vm.month_name_minimun = vm.months_minimun[parseInt(param[0]) - 1];
        vm.month_label = vm.month_name + " de " + param[1];

        data_emit = await vm.getdaysArrayByMonth(vm.mmyy);
        vm.searchText(vm.search);
      } else {
        if (state) {
          vm.ddmmyy = moment(vm.ddmmyy, "DD-MM-YYYY")
            .add(1, "week")
            .format("DD-MM-YYYY");
          vm.mmyy = moment(vm.ddmmyy, "DD-MM-YYYY")
            .add(1, "week")
            .format("MM-YYYY");
          param = vm.ddmmyy.split("-");
        } else {
          vm.ddmmyy = moment(vm.ddmmyy, "DD-MM-YYYY")
            .add(-1, "week")
            .format("DD-MM-YYYY");
          vm.mmyy = moment(vm.ddmmyy, "DD-MM-YYYY")
            .add(-1, "week")
            .format("MM-YYYY");
          param = vm.ddmmyy.split("-");
        }

        vm.month_name = vm.months[parseInt(param[1]) - 1];
        vm.month_name_minimun = vm.months_minimun[parseInt(param[1]) - 1];
        vm.month_label = vm.month_name + " de " + param[2];

        data_emit = await vm.getdaysArrayByWeek(vm.ddmmyy);
        vm.searchTextWeek(vm.search);
      }
    } else {
      if (state) {
        vm.mmyy = moment("1-" + vm.mmyy, "DD-MM-YYYY")
          .add(1, "month")
          .format("MM-YYYY");
        param = vm.mmyy.split("-");
      } else {
        vm.mmyy =moment("1-" + vm.mmyy, "DD-MM-YYYY")
          .add(-1, "month")
          .format("MM-YYYY");
        param = vm.mmyy.split("-");
      }
      vm.month_name = vm.months[parseInt(param[0]) - 1];
      vm.month_name_minimun = vm.months_minimun[parseInt(param[0]) - 1];
      vm.month_label = vm.month_name + " de " + param[1];
      vm.setDataList(vm.mmyy);
    }

    vm.$emit("setmonth", data_emit);
  },

  weekCompare(dataWeek, data) {
    var vm = this;
    moment.locale("es");
    let dataWeek_new = [...dataWeek];
    vm.state_data = false;
    dataWeek_new.forEach((element, index) => {
      let data_push = [];
      data.forEach(elementData => {
        elementData.width = 100
        let state = moment(element.date, "DD-MM-YYYY")
          .isSame(elementData.date, "DD-MM-YYYY");
        if (state) {
          //pertenece al día
          let star_hours_number = elementData.star_hours_number.split(":");
          let end_hours_number = elementData.end_hours_number.split(":");
          elementData.star_date_px = parseInt(star_hours_number[0]) * 30 + (parseInt(star_hours_number[1]) / 60) * 30; 
          elementData.end_hours_number_px = parseInt(end_hours_number[0]) * 30 + (parseInt(end_hours_number[1]) / 60) * 30 - elementData.star_date_px;
          data_push.push(elementData);
        }
      });
      dataWeek_new[index].data = data_push;
    });

      dataWeek_new.forEach(elementX => { //semana
      let data = []
      elementX.data.forEach((elementY,index) => { //día
      dataWeek_new.forEach(elementZ => { //comparativa
        elementZ.data.forEach(elementO => { 
          if ( elementY.star_date_px ==  elementO.star_date_px && elementY.end_hours_number_px == elementO.end_hours_number_px) {
              data.push(index)
          } else if ( elementY.star_date_px > elementO.star_date_px &&  elementY.star_date_px < (elementO.star_date_px + elementO.end_hours_number_px)) {
              elementY.width = elementY.width - 2 
          }
        });
      })
      
    })

    let data_format = []
    data.forEach(dataElemX => {
      let count = 0
      data.forEach(dataElemY => {
        if ( dataElemX == dataElemY) {
            count += 1
        }

      });
      if (count > 1) {
        data_format.push(dataElemX)
      }
    });

    data_format = [...new Set(data_format)]  
    let width = 100/data_format.length
    data_format.forEach((elementData ,indexdata) => {
      elementX.data[elementData].width  = width
      elementX.data[elementData].left = width*indexdata
    });
    
  })

    vm.days_array_week = dataWeek_new;
    vm.state_data = true;
  },

  compare(dataMonth, data) {
    var vm = this;
    moment.locale("es");
    vm.state_data = false;

    let dataMonth_new = [...dataMonth];

    dataMonth_new.forEach((element, index) => {
      let data_push = [];
      for (let data_index = 0; data_index < data.length; data_index++) {
        let state = moment(element.date, "DD-MM-YYYY")
          .isSame(data[data_index].date, "DD-MM-YYYY");
        if (state) {
          data_push.push(data[data_index]);
        }
      }

      if (data_push.length > 2) {
        element.data = [data_push[0], data_push[1]];
        element.more_than_two = true;
        element.count = data_push.length - 2;
        let ids_data = [];

        for (
          let index_push = 1;
          index_push < data_push.length;
          index_push++
        ) {
          ids_data.push(data_push[index_push]);
        }

        element.data[1].ids_data = ids_data;
      } else {
        dataMonth_new[index].data = [...data_push];
      }
    });

    vm.days_array = dataMonth_new;
    vm.state_data = true;
  },
  searchText(param) {
    var vm = this;
    vm.state_menu = false;
    vm.state_menu_week = false;
 
    let data = vm.calendar_data_new.filter(function(item) {
      let text = ( item.text == undefined ||  item.text == null ) ? '' : item.text.toLowerCase()
      let title = ( item.title == undefined ||  item.title == null ) ? '' : item.title.toLowerCase()
      let date = ( item.date == undefined ||  item.date == null ) ? '' : item.date.toLowerCase()
      let star_hours = ( item.star_hours == undefined ||  item.star_hours == null ) ? '' : item.star_hours.toLowerCase()
      let end_hours = ( item.end_hours == undefined ||  item.end_hours == null ) ? '' : item.end_hours.toLowerCase()
      let responsable = ( item.academic_coordination_person == undefined ||  item.academic_coordination_person == null ) ? '' : item.academic_coordination_person.toLowerCase()
      if (
        text.toLowerCase().search(param) != -1 ||
        title.toLowerCase().search(param) != -1 ||
        date.toLowerCase().search(param) != -1 ||
        star_hours.toLowerCase().search(param) != -1 ||
        end_hours.toLowerCase().search(param) != -1 ||
        responsable.search(param) != -1
      ) {
        return item;
      }
    });
    vm.compare(vm.calendar_dataMonth, data);
  },

  searchTextWeek(param) {
    var vm = this;
    vm.state_menu = false;
    vm.state_menu_week = false; 
    let data = vm.calendar_data_new.filter(function(item) {
      let text = ( item.text == undefined ||  item.text == null ) ? '' : item.text.toLowerCase()
      let title = ( item.title == undefined ||  item.title == null ) ? '' : item.title.toLowerCase()
      let date = ( item.date == undefined ||  item.date == null ) ? '' : item.date.toLowerCase()
      let star_hours = ( item.star_hours == undefined ||  item.star_hours == null ) ? '' : item.star_hours.toLowerCase()
      let end_hours = ( item.end_hours == undefined ||  item.end_hours == null ) ? '' : item.end_hours.toLowerCase()
      let responsable = ( item.academic_coordination_person == undefined ||  item.academic_coordination_person == null ) ? '' : item.academic_coordination_person.toLowerCase()
      if (
        text.toLowerCase().search(param) != -1 ||
        title.toLowerCase().search(param) != -1 ||
        date.toLowerCase().search(param) != -1 ||
        star_hours.toLowerCase().search(param) != -1 ||
        end_hours.toLowerCase().search(param) != -1 ||
        responsable.search(param) != -1
      ) {
        return item;
      }
    });
    vm.weekCompare(vm.days_array_week, data);
  },

  saveSubMenu(data) {
    this.$emit("savesubmenu", data);
  },

  alertSubMenu(data) {
    this.$emit("alertsubmenu", data);
  },

  synchronizeCalendar() {
    this.$emit("synchronizecalendar");
  }
}
}

var Card = Vue.component("card", optionsCard);


var CardWeek = Vue.component("card-week",  optionsCardWeek);

var List = Vue.component("list", optionsList);


var Menu = Vue.component("menu-item", optionsMenu)
const gestures = ['tap', 'pan', 'pinch', 'press', 'rotate', 'swipe']
const subGestures = ['panstart', 'panend', 'panmove', 'pancancel', 'pinchstart', 'pinchmove', 'pinchend', 'pinchcancel', 'pinchin', 'pinchout', 'pressup', 'rotatestart', 'rotatemove', 'rotateend', 'rotatecancel']
const directions = ['up', 'down', 'left', 'right', 'horizontal', 'vertical', 'all']
var config = {}
var customEvents = {}
Vue.directive('hammer', {
  bind: (el, binding) => {
    if (!el.hammer) {
      el.hammer = new Hammer.Manager(el)
    }
    const mc = el.hammer

    // determine event type
    const event = binding.arg
    if (!event) {
      console.warn('[vue-hammer] event type argument is required.')
    }
    el.__hammerConfig = el.__hammerConfig || {}
    el.__hammerConfig[event] = {}

    const direction = binding.modifiers
    el.__hammerConfig[event].direction = el.__hammerConfig[event].direction || []
    if (Object.keys(direction).length) {
      Object.keys(direction)
        .filter(keyName => binding.modifiers[keyName])
        .forEach(keyName => {
          const elDirectionArray = el.__hammerConfig[event].direction
          if (elDirectionArray.indexOf(keyName) === -1) {
            elDirectionArray.push(String(keyName))
          }
        })
    }

    let recognizerType,
      recognizer

    if (customEvents[event]) {
      // custom event
      const custom = customEvents[event]
      recognizerType = custom.type
      recognizer = new Hammer[capitalize(recognizerType)](custom)
      recognizer.recognizeWith(mc.recognizers)
      mc.add(recognizer)
    } else {
      // built-in event
      recognizerType = gestures.find(gesture => gesture === event)
      const subGesturesType = subGestures.find(gesture => gesture === event)
      if (!recognizerType && !subGesturesType) {
        console.warn('[vue-hammer] invalid event type: ' + event)
        return
      }
      if (subGesturesType && el.__hammerConfig[subGesturesType].direction.length !== 0) {
        console.warn('[vue-hammer] ' + subGesturesType + ' should not have directions')
      }
      if (!recognizerType) {
        return
      }
      if (recognizerType === 'tap' || recognizerType === 'pinch' || recognizerType === 'press' || recognizerType === 'rotate') {
        if (el.__hammerConfig[recognizerType].direction.length !== 0) {
          throw Error('[vue-hammer] ' + recognizerType + ' should not have directions')
        }
      }
      recognizer = mc.get(recognizerType)
      if (!recognizer) {
        // add recognizer
        recognizer = new Hammer[capitalize(recognizerType)]()
        // make sure multiple recognizers work together...
        recognizer.recognizeWith(mc.recognizers)
        mc.add(recognizer)
      }
      // apply global options
      const globalOptions = config[recognizerType]
      if (globalOptions) {
         guardDirections(globalOptions)
        recognizer.set(globalOptions)
      }
      // apply local options
      const localOptions = el.hammerOptions &&
      el.hammerOptions[recognizerType]
      if (localOptions) {
        guardDirections(localOptions)
        recognizer.set(localOptions)
      }
    }
  },
  inserted: (el, binding) => {
    const mc = el.hammer
    const event = binding.arg
    const eventWithDir = subGestures.find(subGes => subGes === event) ? event :  buildEventWithDirections(event, el.__hammerConfig[event].direction)
    if (mc.handler) {
      mc.off(eventWithDir, mc.handler)
    }
    if (typeof binding.value !== 'function') {
      mc.handler = null
      console.warn(
        '[vue-hammer] invalid handler function for v-hammer: ' +
        binding.arg
      )
    } else {
      mc.on(eventWithDir, (mc.handler = binding.value))
    }
  },
  componentUpdated: (el, binding) => {
    const mc = el.hammer
    const event = binding.arg
    const eventWithDir = subGestures.find(subGes => subGes === event) ? event : buildEventWithDirections(event, el.__hammerConfig[event].direction)
    // teardown old handler
    if (mc.handler) {
      mc.off(eventWithDir, mc.handler)
    }
    if (typeof binding.value !== 'function') {
      mc.handler = null
      console.warn(
        '[vue-hammer] invalid handler function for v-hammer: ' +
        binding.arg
      )
    } else {
      mc.on(eventWithDir, (mc.handler = binding.value))
    }
  },
  unbind: (el, binding) => {
    const mc = el.hammer
    const event = binding.arg
    const eventWithDir = subGestures.find(subGes => subGes === event) ? event : buildEventWithDirections(event, el.__hammerConfig[event].direction)
    if (mc.handler) {
      el.hammer.off(eventWithDir, mc.handler)
    }
    if (!Object.keys(mc.handlers).length) {
      el.hammer.destroy()
      el.hammer = null
    }
  },
})

function guardDirections(options) {
  var dir = options.direction
  if (typeof dir === 'string') {
    var hammerDirection = 'DIRECTION_' + dir.toUpperCase()
    if (directions.indexOf(dir) > -1 && Hammer.hasOwnProperty(hammerDirection)) {
      options.direction = Hammer[hammerDirection]
    } else {
      console.warn('[vue-hammer] invalid direction: ' + dir)
    }
  }
}

function buildEventWithDirections(eventName, directionArray) {
  const f = {}
  directionArray.forEach(dir => {
    dir = dir.toLowerCase()
    if (dir === 'horizontal') {
      f.left = 1
      f.right = 1
    } else if (dir === 'vertical') {
      f.up = 1
      f.down = 1
    } else if (dir === 'all') {
      f.left = 1
      f.right = 1
      f.up = 1
      f.down = 1
    } else {
      f[dir] = 1
    }
  })
  const _directionArray = Object.keys(f)
  if (_directionArray.length === 0) {
    return eventName
  }
  const eventWithDirArray = _directionArray.map(dir => {
    return eventName + dir
  })
  return eventWithDirArray.join(' ')
} 
function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

 Vue.component("sub-menu",  optionSubMenu)
 Vue.component("sub-menu",  optionSubMenuList) 
// Locally Registered Component
var Calendar =  optionsCalendar  

