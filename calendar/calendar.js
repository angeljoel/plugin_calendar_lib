const templateCalendar = `
<div >
    <div class="card m-1 padding-left-right">
      <div>
        <!-- Header -->
        <div class="header-desktop">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 center-movil" style="padding-right:0">
              <button
                @click.prevent="setDay()"
                type="button"
                class="btn btn-md div-border d-inline"
                style="margin-right:1rem"
              >Hoy</button>
              <button
                @click.prevent="setMonth(false)"
                type="button"
                class="btn btn-xs d-inline"
                style="padding:0"
              >
                <i class="material-icons font-size-ico" style="font-size:30px">keyboard_arrow_left</i>
              </button>
              <button
                @click.prevent="setMonth(true)"
                type="button"
                class="btn btn-sm d-inline"
                style="padding:0"
              >
                <i class="material-icons font-size-ico" style="font-size:30px">keyboard_arrow_right</i>
              </button>

              <label for class="ml-4 font-label label-month" v-text="month_label"></label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 inputSearch mt-movil">
              <input
                type="search"
                v-model="search"
                class="form-control background-search form-control-md d-inline"
                style="border:none;font-size:14px;width:100%"
                placeholder="Buscar Coordinadores, Diplomas, Cursos, Seminarios..."
              />
            </div>
            <div
              class="col-lg-5 col-md-5 col-sm-12 center-movil-right mt-movil"
              style="padding-left:0 "
            >
              <div class="d-inline">
                <label for class="font-label mr-2">Vistas:</label>
                <button
                  type="button"
                  class="btn btn-sm div-border mr-1"
                  @click.prevent="setType('month')"
                  :class="[ mode == 'month' ? 'color-panel-select' : '']"
                >
                  <i class="material-icons font-size-ico">calendar_today</i>
                </button>
                <button
                  type="button"
                  class="btn btn-sm div-border mr-4"
                  @click.prevent="setType('list')"
                  :class="[ mode == 'list' ? 'color-panel-select' : '']"
                >
                  <i class="material-icons font-size-ico">list</i>
                </button>

                <select
                  class="form-control form-control-md d-inline font-label vertical-align"
                  style="max-width:5.5rem ; "
                  v-model="type"
                  @change="onChangeType(type)"
                >
                  <option selected value="month">Mes</option>
                  <option value="week">Semana</option>
                </select>
              </div>
              <button
                @click.prevent="synchronizeCalendar()"
                type="button"
                class="btn btn-md font-label d-inline background-search select-color pl-2 ml-4 mt-movil btn-calendar"
              >Sincronizar Calendario</button>
            </div>
          </div>
        </div>
        <div class="header-movil">
          <div class="space-between">
            <div>
              <button class="btn btn-md">
                <i class="material-icons">menu</i>
              </button>
              <label for class="font-label label-month" v-text="  month_label "></label>
            </div>

            <div>
              <div class="btn-group">
                <button
                  type="button"
                  class="btn btn-md"
                  data-toggle="dropdown"
                  data-display="static"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i class="material-icons">more_horiz</i>
                </button>
                <div
                  class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left"
                  style=" right:0.5rem;top:0.5rem;padding:8px 0 0 0"
                >
                  <a
                    class="dropdown-item fw-500"
                    @click.prevent="onChangeType('month'); type = 'month'"
                    href="#"
                  >Mes</a>
                  <a
                    class="dropdown-item fw-500"
                    @click.prevent="onChangeType('week'); type = 'week'"
                    href="#"
                  >Semana</a>
                  <div class="dropdown-divider" style="margin:0;margin-top:8px"></div>
                  <a class="dropdown-item a-calendar" href="#">Agregar a calendario</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end Header -->

        <!-- Body -->
        <transition name="fade" mode="out-in">
          <div v-if="mode == 'month'" style="position:relative">
            <transition name="fade" mode="out-in">
              <div v-if="type == 'month'" key="1">
                <div class="container-dates-label" style="margin-top:0.7rem">
                  <template v-for="elem in days_array_names">
                    <div :key="elem.name" class="header-desktop">
                      <div class="text-uppercase font-date-label">{{elem.nameLg}}</div>
                    </div>
                    <div :key="elem.name" class="header-movil">
                      <div class="text-uppercase font-date-label">{{elem.nameXs}}</div>
                    </div>
                  </template>
                </div>
                <div
                  :class="[ !state_data ? 'container-dates disable-div' : 'container-dates block-div']"
                >
                  <div
                    v-for="(elem,index)  in days_array"
                    :key="index"
                    :class="[(current_day == elem.date) ? 'border-dates-day' : 'border-dates']"
                    :style="{color: elem.color , background: elem.background}"
                  >
                    <img
                      v-show="current_day == elem.date"
                      :src="require('./../public/images/day.svg')"
                      alt
                      style="position: absolute;height:0.8rem;margin-left:-0.02rem; margin-top:-0.50rem;"
                    />
                    <!-- <hr  v-show="current_day == elem.date"  
                      style="border-bottom:0.2rem solid #E84548; margin-top:0rem;border-top:0; margin-bottom:0" 
                    />-->
                    <div class="font-day" v-text="elem.day"></div>

                    <div>
                      <transition-group
                        name="staggered-fade"
                        tag="div"
                        v-bind:css="false"
                        v-on:before-enter="beforeEnter"
                        v-on:after-enter="afterEnter"
                        v-on:enter="enter"
                        v-on:leave="leave"
                        v-on:before-leave="beforeLeave"
                        v-on:after-leave="afterLeave"
                        v-on:leave-cancelled="leaveCancelled"
                        class="container-date-children"
                        :id="[elem.more_than_two ? 'background-more-than-two' : '']"
                      >
                        <card
                          v-for="(param_data,index_data) in elem.data"
                          :index_column="index"
                          class="div-body-date"
                          :key="index_data+ 'card'"
                          :param="param_data"
                          :count="elem.count"
                          :more_than_two="elem.more_than_two"
                          :index="index_data"
                          :color="param_data.color"
                          :background_days="elem.background_days"
                          :background="param_data.background"
                          @getMenu="getMenu"
                        ></card>
                      </transition-group>
                    </div>
                  </div>
                </div>
              </div>
              <!-- week -->
              <div v-else key="2">
                <div
                  class="container-dates-label-week"
                  style="margin-top:0.7rem; padding-right:0.53rem"
                >
                  <div style="width:100%"></div>
                  <template v-for="elem in days_array_week">
                    <div :key="elem.name">
                      <div class="text-uppercase font-date-label-week">{{elem.name.substr(0,3)}}</div>
                      <div class="text-center font-weight-bold">{{elem.day}}</div>
                    </div>
                  </template>
                </div>
                <div class="container-dates-week scroll-global">
                  <div class="div-week-border">
                    <div
                      v-for=" hour  in hours_data"
                      :key="hour.hour"
                      class="div-children-week div-children-week-font"
                    >
                      <span class="text-left hour-format">{{hour.hour}} {{hour.format}}</span>
                    </div>
                  </div>
                  <div
                    v-for="(param,index) in days_array_week"
                    :key="index"
                    class="div-week-border"
                    style="position:relative"
                  >
                    <div
                      v-for="(paramh ,indexhs) in hours_data"
                      :key="indexhs"
                      class="div-children-week"
                    ></div>

                    <img
                      v-show="current_day == param.date"
                      :src="require('./../public/images/day.svg')"
                      :style="{'position': 'absolute','height':'0.8rem','margin-left':'-0.07rem','z-index': '100',
                               'top': current_hour_mm + 'px'}"
                    />

                    <hr
                      v-show="current_day == param.date"
                      :style="{'position': 'absolute','height':'0.1rem' ,
                               'top':( current_hour_mm - 11) + 'px', 'width': '100%', 'background': '#E84548',
                               'z-index': 100}"
                    />
                    <template v-for="(param_data_px,index_px) in param.data">
                      <card-week
                        :key="index_px+'px'"
                        :style="{'width': '100%', 'position':'absolute', 
                                   'height':param_data_px.end_hours_number_px + 'px',
                                   'top': param_data_px.star_date_px + 'px'}"
                        class="div-body-date"
                        :param="param_data_px"
                        :index="index_px"
                        :index_column="index"
                        :color="param_data_px.color"
                        :background_days="param.background_days"
                        :background="param_data_px.background"
                        state_end_date
                        @getMenuWeek="getMenuWeek"
                      />
                    </template>
                  </div>
                </div>
              </div>
              <!-- end week  -->
            </transition>

            <div v-show="state_menu" :style="getMenuPosition">
              <menu-item
                @cancelMenu="state_menu = false"
                :data="menu_data"
                :state_right="state_right"
                :state_movil_menu="state_movil_menu"
                ref="menu-item"
                :icons_sub_menu="icons_sub_menu"
              />
            </div>

            <div v-show="state_menu_week" :style="getMenuPositionWeek">
              <menu-item
                @cancelMenu="state_menu_week = false"
                :data="menu_data"
                :state_right="state_right"
                :state_movil_menu="state_movil_menu"
                ref="menu-item-week"
                :icons_sub_menu="icons_sub_menu"
              />
            </div>
          </div>
          <!-- List -->
          <div v-else style="margin-top:0.7rem">
            <list :data="list_data" 
            @saveSubMenu="saveSubMenu"
            @alertSubMenu="alertSubMenu"
            :icons_sub_menu="icons_sub_menu" />
          </div>
          <!-- end List -->
        </transition>
        <!-- end Body -->
      </div>
    </div>
  </div>
  `

  const optionsCalendar= {
    name: "calendar",
    props:{
        calendar_data: Array,
        icons_sub_menu: Object
    },
    components: {
      card,
      cardWeek,
      menuItem,
      list
    },
    created() {
      this.getDataMonth();
    },
  
    watch: {
      search() {
        var vm = this;
        if (vm.type == "month") {
          vm.searchText(vm.search.toLowerCase());
        } else {
          vm.searchTextWeek(vm.search.toLowerCase());
        }
      }
    },
    data() {
      return {
        index_column: "",
        state_menu: false,
        search: "",
        list: false,
        mode: "month",
        dataMonth: [],
        type: "month",
        month_select: "name",
        calendar_data_new: [],
        days_array: [],
        days_array_week: [],
        month_label: "",
        color_remaining_days: "#AAB7B8",
        color_days: "#212529",
        background_days: "#F0F9FE",
        background_before_days: "#FFFFFF",
        color_border_days: "#FFFFFF",
        // end props
        days_array_names: [
          { nameLg: "DOM", nameXs: "D" },
          { nameLg: "LUN", nameXs: "L" },
          { nameLg: "MAR", nameXs: "M" },
          { nameLg: "MIÉ", nameXs: "M" },
          { nameLg: "JUE", nameXs: "J" },
          { nameLg: "VIE", nameXs: "V" },
          { nameLg: "SÁB", nameXs: "S" }
        ],
        days_array_names_week: [],
        months: [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Setiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ],
        months_minimun: [
          "ENE",
          "FEB",
          "MAR",
          "ABR",
          "MAY",
          "JUN",
          "JUL",
          "AGO",
          "SET",
          "OCT",
          "NOV",
          "DIC"
        ],
        mmyy: "",
        ddmmyy: "",
        year: "",
        month: "",
        current_month: "",
        current_date: [],
        state_data: false,
        month_name: "",
        month_name_minimun: "",
        month_name_week: "",
        month_name_minimun_week: "",
        hours_data: [
          {
            hour: "",
            data: []
          },
          {
            hour: 1,
            data: [],
            format: "AM"
          },
          {
            hour: 2,
            data: [],
            format: "AM"
          },
          {
            hour: 3,
            data: [],
            format: "AM"
          },
          {
            hour: 4,
            data: [],
            format: "AM"
          },
          {
            hour: 5,
            data: [],
            format: "AM"
          },
          {
            hour: 6,
            data: [],
            format: "AM"
          },
          {
            hour: 7,
            data: [],
            format: "AM"
          },
          {
            hour: 8,
            data: [],
            format: "AM"
          },
          {
            hour: 9,
            data: [],
            format: "AM"
          },
          {
            hour: 10,
            data: [],
            format: "AM"
          },
          {
            hour: 11,
            data: [],
            format: "AM"
          },
          {
            hour: 12,
            data: [],
            format: "PM"
          },
          {
            hour: 13,
            data: [],
            format: "PM"
          },
          {
            hour: 14,
            data: [],
            format: "PM"
          },
          {
            hour: 15,
            data: [],
            format: "PM"
          },
          {
            hour: 16,
            data: [],
            format: "PM"
          },
          {
            hour: 17,
            data: [],
            format: "PM"
          },
          {
            hour: 18,
            data: [],
            format: "PM"
          },
          {
            hour: 19,
            data: [],
            format: "PM"
          },
          {
            hour: 20,
            data: [],
            format: "PM"
          },
          {
            hour: 21,
            data: [],
            format: "PM"
          },
          {
            hour: 22,
            data: [],
            format: "PM"
          },
          {
            hour: 23,
            data: [],
            format: "PM"
          }
        ],
        menu_data: [],
        list_data: [],
        state_movil_menu: false,
        state_right: false,
        index_column_index: 0,
        state_menu_week: false
      };
    },
    computed: {
      getMenuPosition() {
        var vm = this;
        let top = Number;
        let left = Number;
        let right = false;
        let index_column = vm.index_column.index_column + 1;
        let index = vm.index_column.index;
        let left_number = 0;
        if (index_column < 8) {
          //primera fila
          if (index_column == 6 || index_column == 7) {
            //columnas penultima y ultima
            top = -70;
            left = (7.52 - index_column) * 14.28;
            right = true;
          } else {
            top = -70;
            left = index_column * 14.28;
          }
        } else {
          //resto de filas
          let top_number = Math.ceil(parseInt(index_column) / 7) - 1;
          left_number = parseInt(index_column) % 7;
          left_number = left_number == 0 ? 7 : left_number;
          if (left_number == 6 || left_number == 7) {
            //columnas penultima y ultima
            right = true;
            top = top_number * 70;
            left = (7.52 - left_number) * 14.28;
          } else {
            top = top_number * 70;
            left = left_number * 14.28;
          }
        }
  
        vm.state_right = right;
        if (left_number == 5) {
          vm.state_right = true;
        }
  
        vm.state_movil_menu = window.innerWidth < 768 ? true : false;
        if (window.innerWidth < 768) {
          // movil
          return `width:96%;position:absolute; top:5%;left:2%; right:2%;background:white;border-radius: 0.5rem;
                        filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
        } else if (right) {
          //desktop
          if (index == 0) {
            left = left + 7.4;
          }
          return `width:17rem;position:absolute;top:${top}px;right:${left}%;background:white;border-radius: 0.5rem;
                         filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
        } else {
          if (index == 0) {
            left = left - 6.8;
          }
          return `width:17rem;position:absolute;top:${top}px;left:${left}%;background:white;border-radius: 0.5rem;
                         filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
        }
      },
  
      getMenuPositionWeek() {
        var vm = this;
        let top = Number;
        let left = Number;
        let right = false;
        let index_column = vm.index_column.index_column + 1;
        let index = vm.index_column.index;
        let left_number = 0;
  
        if (index_column < 8) {
          //primera fila
          if (index_column == 6 || index_column == 7) {
            //columnas penultima y ultima
  
            left = (7.52 - index_column) * 13.7;
            right = true;
          } else {
            left = index_column * 13.7;
          }
          left_number = index_column;
        } else {
          //resto de filas
          let top_number = Math.ceil(parseInt(index_column) / 7) - 1;
          left_number = parseInt(index_column) % 7;
          left_number = left_number == 0 ? 7 : left_number;
          if (left_number == 6 || left_number == 7) {
            //columnas penultima y ultima
            right = true;
  
            left = (7.52 - left_number) * 13.7;
          } else {
            left = left_number * 13.7;
          }
        }
  
        vm.state_right = right;
        if (left_number >= 5) {
          vm.state_right = true;
        }
  
        top = vm.index_column.star_date_px;
  
        vm.state_movil_menu = window.innerWidth < 768 ? true : false;
        if (vm.state_movil_menu) {
          // movil
          return `width:96%;position:absolute; top:5%;left:2%; right:2%;background:white;border-radius: 0.5rem;
                        filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
        } else if (right) {
          //desktop
  
          left = left + 9;
  
          return `width:17rem;position:absolute;top:${top}px;right:${left}%;background:white;border-radius: 0.5rem;
                         filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
        } else {
          left = left + 5;
          return `width:17rem;position:absolute;top:${top}px;left:${left}%;background:white;border-radius: 0.5rem;
                         filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, .2))`;
        }
      },
  
      current_day() {
        var vm = this;
        return vm.$moment().format("DD-MM-YYYY");
      },
  
      current_hour_mm() {
        var vm = this;
        let date = vm.$moment().format("H:mm");
        date = date.split(":");
        return parseInt(date[0]) * 30 + (parseInt(date[1]) / 60) * 30;
      }
    },
  
    methods: {
      getMenu(param) {
        let index = param.index;
        let index_column = param.index_column;
        var vm = this;
  
        if (vm.index_column_index != index_column + "-" + index) {
          vm.$refs["menu-item"].state_menu = false;
        }
  
        vm.index_column_index = index_column + "-" + index;
        let data =
          vm.days_array[index_column].data[index].ids_data == undefined
            ? vm.days_array[index_column].data
            : vm.days_array[index_column].data[index].ids_data;
  
        if (index == 0) {
          data = [vm.days_array[index_column].data[0]];
        }
  
        if (data != undefined) {
          let ids = [];
          let menu_data = [];
          vm.state_menu = true;
          let days_names = vm.$moment.weekdays();
          vm.index_column = param;
          for (let index = 0; index < data.length; index++) {
            ids.push(data[index].id);
          }
  
          ids = ids.filter(function(value, index, self) {
            //eliminar duplicados
            return self.indexOf(value) === index;
          });
  
          ids.forEach((element, index) => {
            menu_data.push({
              sub_title: "",
              color: "",
              data: []
            });
            data.forEach(element_data => {
              if (element_data.id == element) {
                if (menu_data[index].data.length == 0) {
                  menu_data[index].sub_title = element_data.sub_title;
                  menu_data[index].color = element_data.color;
                  menu_data[index].data = [element_data];
                } else {
                  menu_data[index].data.push(element_data);
                }
              }
            });
          });
          let name =
            vm.days_array[index_column].name.charAt(0).toUpperCase() +
            vm.days_array[index_column].name.slice(1);
          let date = vm.days_array[index_column].date.split("-");
  
          let date_name = `${name}, ${date[0]} de ${
            vm.months[parseInt(date[1]) - 1]
          } de ${date[2]}`;
          menu_data.date_name = date_name;
          vm.menu_data = menu_data;
        }
      },
  
      getMenuWeek(param) {
        let index = param.index;
        let index_column = param.index_column;
        var vm = this;
  
        if (vm.index_column_index != index_column + "-" + index) {
          vm.$refs["menu-item-week"].state_menu = false;
        }
  
        vm.index_column_index = index_column + "-" + index;
        let data = [param.data];
        if (data != undefined) {
          let ids = [];
          let menu_data = [];
          vm.state_menu_week = true;
          let days_names = vm.$moment.weekdays();
          vm.index_column = param;
          for (let index = 0; index < data.length; index++) {
            ids.push(data[index].id);
          }
  
          ids = ids.filter(function(value, index, self) {
            //eliminar duplicados
            return self.indexOf(value) === index;
          });
  
          ids.forEach((element, index) => {
            menu_data.push({
              sub_title: "",
              color: "",
              data: []
            });
            data.forEach(element_data => {
              if (element_data.id == element) {
                if (menu_data[index].data.length == 0) {
                  menu_data[index].sub_title = element_data.sub_title;
                  menu_data[index].color = element_data.color;
                  menu_data[index].data = [element_data];
                } else {
                  menu_data[index].data.push(element_data);
                }
              }
            });
          });
          let name =
            vm.days_array[index_column].name.charAt(0).toUpperCase() +
            vm.days_array[index_column].name.slice(1);
          let date = vm.days_array[index_column].date.split("-");
  
          let date_name = `${name}, ${date[0]} de ${
            vm.months[parseInt(date[1]) - 1]
          } de ${date[2]}`;
          menu_data.date_name = date_name;
          vm.menu_data = menu_data;
        }
      },
  
      beforeLeave(el, done) {
        el.style.opacity = 0;
      },
  
      afterLeave(el, done) {
        el.style.opacity = 0.6;
      },
  
      leaveCancelled(el, done) {},
  
      afterEnter(el, done) {},
  
      beforeEnter(el, done) {
        el.style.opacity = 0.3;
      },
  
      enter(el, done) {
        setTimeout(() => {
          el.style.opacity = 1;
        }, 100);
      },
  
      leave(el, done) {
        // console.log(el.dataset )
        //  setTimeout(() => {
        //    el.style.opacity = 0
        // }, 300);
      },
  
      setType(type) {
        var vm = this;
        vm.mode = type;
        if (type == "list") {
          vm.setDataList(vm.mmyy);
        }
      },
  
      setDataList(mmyy) {
        var vm = this;
        let ids = [];
        let programs = [];
        let programs_courses = [];
        let courses = [];
        let list_data = [];
        let data_list = [];
        let star_date = "01-" + mmyy;
        star_date = vm.$moment(star_date, "DD-MM-YYYY").format("YYYY-MM-DD");
        let end_date = vm
          .$moment(star_date, "YYYY-MM-DD")
          .add(1, "month")
          .format("YYYY-MM-DD");
        end_date = vm
          .$moment(end_date, "YYYY-MM-DD")
          .add(-1, "day")
          .format("YYYY-MM-DD");
  
        for (let index = 0; index < vm.calendar_data_new.length; index++) {
          let date = vm.calendar_data_new[index].date;
          let after_date = vm.$moment(date).isSameOrAfter(star_date);
          let before_date = vm.$moment(date).isSameOrBefore(end_date);
  
          if (after_date && before_date) {
            let state_exist_program = false;
            if (vm.calendar_data_new[index].id != null) {
              //programas
  
              for (
                let index_prog = 0;
                index_prog < programs.length;
                index_prog++
              ) {
                if (programs[index_prog].id == vm.calendar_data_new[index].id) {
                  state_exist_program = true;
                  break;
                }
              }
  
              if (!state_exist_program) {
                programs.push({
                  program: vm.calendar_data_new[index].program,
                  title: vm.calendar_data_new[index].title,
                  id: vm.calendar_data_new[index].id,
                  place: vm.calendar_data_new[index].place,
                  coordinator: vm.calendar_data_new[index].coordinator,
                  type: "P",
                  courses: []
                });
              }
            } else {
              // cursos
              let state_exist_program_course = false;
              for (
                let index_prog = 0;
                index_prog < programs_courses.length;
                index_prog++
              ) {
                if (
                  programs_courses[index_prog].sub_title ==
                    vm.calendar_data_new[index].sub_title &&
                  programs_courses[index_prog].id == null
                ) {
                  state_exist_program_course = true;
                  break;
                }
              }
  
              if (!state_exist_program_course) {
                programs_courses.push({
                  type: "C",
                  modality: vm.calendar_data_new[index].modality,
                  id: vm.calendar_data_new[index].id,
                  sub_title: vm.calendar_data_new[index].sub_title,
                  title: vm.calendar_data_new[index].title,
                  text: vm.calendar_data_new[index].text,
                  color: vm.calendar_data_new[index].color,
                  background: vm.calendar_data_new[index].background,
                  events: []
                });
              }
            }
  
            let state_exist_cursos = false;
            for (
              let index_courses = 0;
              index_courses < courses.length;
              index_courses++
            ) {
              if (
                courses[index_courses].sub_title == vm.calendar_data_new[index].sub_title
              ) {
                state_exist_cursos = true;
                break;
              }
            }
  
            if (!state_exist_cursos) {
              //todos los cursos
              courses.push({
                id: vm.calendar_data_new[index].id,
                sub_title: vm.calendar_data_new[index].sub_title,
                title: vm.calendar_data_new[index].title,
                text: vm.calendar_data_new[index].text,
                modality: vm.calendar_data_new[index].modality,
                color: vm.calendar_data_new[index].color,
                background: vm.calendar_data_new[index].background
              });
            }
  
            let date_name = vm.calendar_data_new[index].date.split("-");
            data_list.push({
              id: vm.calendar_data_new[index].id,
              _id: vm.calendar_data_new[index]._id,
              title: vm.calendar_data_new[index].title,
              star_hours: vm.calendar_data_new[index].star_hours,
              star_hours_number: vm.calendar_data_new[index].star_hours_number,
              end_hours: vm.calendar_data_new[index].end_hours,
              end_hours_number: vm.calendar_data_new[index].end_hours_number,
              name: vm.calendar_data_new[index].name,
              dateY: date_name[0],
              dateM: date_name[1],
              dateD: date_name[2],
              date: vm.calendar_data_new[index].date,
              name_month: vm.months[parseInt(date_name[1]) - 1],
              description: vm.calendar_data_new[index].description,
              text: vm.calendar_data_new[index].text,
              modality: vm.calendar_data_new[index].modality,
              academic_coordination_person: vm.calendar_data_new[index].academic_coordination_person,
              academic_coordination_phone: vm.calendar_data_new[index].academic_coordination_phone,
              academic_coordination_email: vm.calendar_data_new[index].academic_coordination_email,
              help_desk_person: vm.calendar_data_new[index].help_desk_person,
              help_desk_phone: vm.calendar_data_new[index].help_desk_phone,
              place: vm.calendar_data_new[index].place,
              color: vm.calendar_data_new[index].color,
              background: vm.calendar_data_new[index].background
            });
          }
        }
  
        programs.forEach(element => {
          courses.forEach((element_courses, indexC) => {
            if (element.id == element_courses.id) {
              let data = [];
              for (let index = 0; index < data_list.length; index++) {
                if (
                  element_courses.title == data_list[index].title &&
                  element.id == data_list[index].id
                ) {
                  data.push(data_list[index]);
                }
              }
              element_courses.star_date = vm
                .$moment(data[0].date, "YYYY-MM-DD")
                .format("DD/MM/YYYY");
              element_courses.end_date = vm
                .$moment(data[data.length - 1].date, "YYYY-MM-DD")
                .format("DD/MM/YYYY");
              element_courses.events = data;
              element.courses.push(element_courses);
            }
          });
        });
  
        programs_courses.forEach((element_courses, indexC) => {
          let data = [];
          for (let index = 0; index < data_list.length; index++) {
            if (
              element_courses.title == data_list[index].title &&
              data_list[index].id == null
            ) {
              data.push(data_list[index]);
            }
          }
          element_courses.star_date = vm
            .$moment(data[0].date, "YYYY-MM-DD")
            .format("DD/MM/YYYY");
          element_courses.end_date = vm
            .$moment(data[data.length - 1].date, "YYYY-MM-DD")
            .format("DD/MM/YYYY");
          element_courses.events = data;
        });
  
        for (let index = 0; index < programs.length; index++) {
          programs[index].star_date = programs[index].courses[0].star_date;
          programs[index].end_date =
            programs[index].courses[programs[index].courses.length - 1].end_date;
        }
  
        programs = programs.concat(programs_courses);
        vm.list_data = programs;
      },
  
      async onChangeType(type) {
        var vm = this;
        vm.state_menu = false;
        vm.state_menu_week = false;
        if (type != "month" && vm.days_array_week.length == 0) {
          vm.weekCompare(await vm.getdaysArrayByWeek(vm.ddmmyy), vm.calendar_data_new);
        } else {
          vm.compare(await vm.getdaysArrayByMonth(vm.mmyy), vm.calendar_data_new);
        }
      },
  
      getdaysArrayByMonth(mmyy) {
        var vm = this;
        vm.$moment.locale("es");
        let days = vm.$moment(mmyy, "MM-YYYY").daysInMonth(); //cantidad de días del mes
        let date_array = []; //array de mes a llenar
        let days_names = vm.$moment.weekdays();
        let current_date = vm.$moment().format("YYYY-MM-DD");
        for (let index = 0; index < days; index++) {
          // fechas de mes actual
          let index_date =
            index < 9 ? "0" + String(index + 1) : String(index + 1);
          let date = index_date + "-" + mmyy;
          let date_original = vm.$moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
  
          let name = days_names[vm.$moment(date, "DD-MM-YYYY").day()];
  
          date_array.push({
            date: date,
            date_original: date_original,
            name: name,
            day: String(index + 1),
            background_days: true,
            data: [],
            count: 0,
            more_than_two: false
          });
        }
  
        if (date_array[0].name != "domingo") {
          // agregar días faltas hacía atras
          date_array[0].day = vm.month_name_minimun + " " + date_array[0].day;
          for (let index = 0; index < 11; index++) {
            let date = vm
              .$moment("1-" + mmyy, "DD-MM-YYYY")
              .add(-1 * (index + 1), "day")
              .format("DD-MM-YYYY");
            let name = days_names[vm.$moment(date, "DD-MM-YYYY").day()];
            let date_original = vm
              .$moment(date, "DD-MM-YYYY")
              .format("YYYY-MM-DD");
  
            date_array.unshift({
              date: date,
              date_original: date_original,
              name: name,
              day: parseInt(date.substr(0, 2)),
              background_days: true,
              data: [],
              count: 0,
              more_than_two: false
            });
            if (name == "domingo") {
              break;
            }
          }
        }
  
        let count_after = 42 - date_array.length;
        for (let index = 0; index < count_after; index++) {
          // agregar días faltantes hacia adelante
          let date = vm
            .$moment(date_array[date_array.length - 1].date, "DD-MM-YYYY")
            .add(1, "day")
            .format("DD-MM-YYYY");
  
          let name = days_names[vm.$moment(date, "DD-MM-YYYY").day()];
          date_array.push({
            date: date,
            date_original: vm.$moment(date, "DD-MM-YYYY").format("YYYY-MM-DD"),
            name: name,
            day: parseInt(date.substr(0, 2)),
            background_days: true,
            data: [],
            count: 0,
            more_than_two: false
          });
        }
  
        date_array.forEach(element => {
          //color antes de fecha actual
          let state = vm
            .$moment(element.date_original, "YYYY-MM-DD")
            .isSameOrAfter(current_date, "YYYY-MM-DD");
  
          element.background_days = state;
        });
        vm.calendar_dataMonth = date_array;
        return new Promise(resolve => {
          resolve(date_array);
        });
      },
  
      getdaysArrayByWeek(ddmmyy) {
        var vm = this;
        let days_names = vm.$moment.weekdays();
        let date = ddmmyy;
        let name = days_names[vm.$moment(date, "DD-MM-YYYY").day()];
        let date_array_week = [];
  
        let current_date = vm.$moment().format("YYYY-MM-DD");
  
        date_array_week.push({
          date: date,
          day: vm.$moment(date, "DD-MM-YYYY").format("DD"),
          date_original: vm.$moment(date, "DD-MM-YYYY").format("YYYY-MM-DD"),
          name: name,
          background_days: true,
          data: [],
          count: 0,
          more_than_two: false
        });
  
        if (date_array_week[0].name != "domingo") {
          // agregar días faltas hacía atras
  
          for (let index = 0; index < 7; index++) {
            let before_date = vm
              .$moment(date, "DD-MM-YYYY")
              .add(-1 * (index + 1), "day")
              .format("DD-MM-YYYY");
  
            let name = days_names[vm.$moment(before_date, "DD-MM-YYYY").day()];
            let date_original = vm
              .$moment(before_date, "DD-MM-YYYY")
              .format("YYYY-MM-DD");
  
            date_array_week.unshift({
              date: before_date,
              day: vm.$moment(before_date, "DD-MM-YYYY").format("DD"),
              date_original: date_original,
              name: name,
              background_days: true,
              data: [],
              count: 0,
              more_than_two: false
            });
            if (name == "domingo") {
              break;
            }
          }
        }
  
        let count_after = 7 - date_array_week.length;
        for (let index = 0; index < count_after; index++) {
          // agregar días faltantes hacia adelante
          let after_date = vm
            .$moment(
              date_array_week[date_array_week.length - 1].date,
              "DD-MM-YYYY"
            )
            .add(1, "day")
            .format("DD-MM-YYYY");
  
          let name = days_names[vm.$moment(after_date, "DD-MM-YYYY").day()];
          date_array_week.push({
            date: after_date,
            day: vm.$moment(after_date, "DD-MM-YYYY").format("DD"),
            date_original: vm
              .$moment(after_date, "DD-MM-YYYY")
              .format("YYYY-MM-DD"),
            name: name,
            background_days: true,
            count: 0,
            more_than_two: false
          });
        }
  
        date_array_week.forEach(element => {
          //color antes de fecha actual
          let state = vm
            .$moment(element.date_original, "YYYY-MM-DD")
            .isSameOrAfter(current_date, "YYYY-MM-DD");
  
          element.background_days = state;
        });
  
        vm.days_array_week = date_array_week;
  
        return new Promise(resolve => {
          resolve(date_array_week);
        });
        //set horas restantes
      },
  
      async onChangeMonth(mm) {
        var vm = this;
        let a = vm.mmyy.split("-")[1] + "-" + mm;
        if (
          vm
            .$moment(a, "YYYY-MM-DD")
            .isSame(vm.$moment().format("YYYY-MM"), "YYYY-MM")
        ) {
          vm.month_select = "name";
        }
        vm.$moment.locale("es");
        let yy = vm.mmyy.split("-")[1];
        let param = vm.mmyy.split("-");
  
        vm.month = mm; //mes y año actual
        vm.month_name = vm.months[parseInt(mm) - 1];
        vm.month_name_minimun = vm.months_minimun[parseInt(mm) - 1];
        vm.month_label = vm.month_name + " de " + yy;
        vm.mmyy = mm + "-" + yy;
        await vm.getdaysArrayByMonth(vm.mmyy);
        vm.searchText(vm.search);
      },
  
      async setDay() {
        var vm = this;
        vm.$moment.locale("es");
        if (vm.type == "month") {
          vm.month_select = "name";
          vm.mmyy = String(vm.$moment().format("MM-YYYY"));
          let param = vm.mmyy.split("-");
  
          vm.month = vm.$moment().format("MM"); //mes y año actual
          vm.month_name = vm.months[parseInt(param[0]) - 1];
          vm.month_name_minimun = vm.months_minimun[parseInt(param[0]) - 1];
          vm.month_label = vm.month_name + " de " + param[1];
  
          await vm.getdaysArrayByMonth(vm.mmyy);
          vm.searchText(vm.search);
        } else {
          vm.month_select = "name";
          vm.ddmmyy = String(vm.$moment().format("DD-MM-YYYY"));
          let param = vm.ddmmyy.split("-");
  
          vm.month = vm.$moment().format("MM"); //mes y año actual
          vm.month_name_week = vm.months[parseInt(param[1]) - 1];
          vm.month_name_minimun_week = vm.months_minimun[parseInt(param[1]) - 1];
          vm.month_label = vm.month_name_week + " de " + param[2];
  
          await vm.getdaysArrayByWeek(vm.ddmmyy);
          vm.searchTextWeek(vm.search);
        }
      },
  
      async getDataMonth() {
        //Formateo de data
        var vm = this;
        moment.locale("es");
        //vm.months = vm.$moment.months(); //nombres de meses del año
  
        vm.mmyy =   ( vm.mmyy ) ? vm.mmyy :  String(moment().format("MM-YYYY"));
        vm.ddmmyy = (vm.ddmmyy) ? vm.ddmmyy : String(moment().format("DD-MM-YYYY"));
        let param = vm.mmyy.split("-");
  
        vm.month = moment().format("MM"); //mes actual
        vm.current_month = moment().format("MM"); //mes  actual no mutable
        vm.month_name = vm.months[parseInt(param[0]) - 1];
        vm.month_name_minimun = vm.months_minimun[parseInt(param[0]) - 1];
        vm.month_label = vm.month_name + " de " + param[1];
        let data_new = [];
  
        let days_names = moment.weekdays();
  
        for (let index = 0; index < vm.calendar_data.length; index++) {
          let data = vm.calendar_data[index];
          for (
            let indexEvent = 0;
            indexEvent < vm.calendar_data[index].events.length;
            indexEvent++
          ) {
            let data_event = vm.calendar_data[index].events[indexEvent];
            let date_name = moment
              .unix(data_event.star_date)
              .format("YYYY-MM-DD");
            let name = days_names[moment(date_name, "YYYY-MM-DD").day()];
            name = name.charAt(0).toUpperCase() + name.slice(1)
            
            let start_new_format_date = moment
              .unix(data_event.star_date)
              .format("YYYY-MM-DD, h:mm a , H:mm")
              .split(",");
  
            let end_new_format_date = moment
              .unix(data_event.end_date)
              .format("YYYY-MM-DD, h:mm a , H:mm")
              .split(",");
            let state = moment(end_new_format_date[0], "YYYY-MM-DD")
              .isSameOrAfter(start_new_format_date[0], "YYYY-MM-DD");
  
            // Segun el tipo
              switch ( data_event.type ) {
                 case "C":
                  data_event.title = `<i style="font-size:11px" class="material-icons">${ vm.icon_class_default }</i> ${vm.class_default}`
                  break;
                 case "T":
                  data_event.title =  `<i style="font-size:11px" class="material-icons">${ vm.icon_book_default }</i> ${vm.book_default}`
                  break;
                 case "E":
                  data_event.title =  `<i style="font-size:11px" class="material-icons">${ vm.icon_evaluation_default }</i> ${vm.evaluation_default}` 
                 break;
                 default:
                  data_event.title =   `<i style="font-size:11px" class="material-icons">${ vm.data_event.icon_title }</i> ${vm.data_event.title}`
                 break;
              }
              
            // fin segun tipo
  
            if( data.color == '' || data.color == null || data.color ==  undefined ){
              let position_color = Math.floor(Math.random() * 9);
              data.color = colors[position_color].color
              data.color_title = colors[position_color].color_title
              data.background = colors[position_color].background
            }
  
            if (state) {
              // La fecha end, debe ser mayor o igual a la inicial para ser considerada
              let difference = moment(end_new_format_date[0])
                .diff(moment(start_new_format_date[0]), "days");
  
              if (difference > 0) {
                // Separación de fechas, si abarca más de un día
                difference = difference + 1;
                let date_new = ''
                for (
                  let index_difference = 0;
                  index_difference < difference;
                  index_difference++
                ) {
                  //Diviendo en bloques
                  if (index_difference == 0) {
                    data_new.push({
                      name: name,
                      classroom: data_event.classroom,
                      eventid: data_event.id,
                      responsable_id: data.responsable_id,
                      program_dstart:  moment.unix( data.program_dstart ).format("DD/MM/YYYY"), 
                      program_dend:  moment.unix( data.program_dend ).format("DD/MM/YYYY"),
                      course_dstart: moment.unix( data.course_dstart ).format("DD/MM/YYYY"),
                      course_dend: moment.unix( data.course_dend ).format("DD/MM/YYYY"),
                      modality: data_event.modality,
                      docente: data.docente,
                      type: data.type,
                      academic_coordination_person: data.academic_coordination_person,
                      academic_coordination_phone: data.academic_coordination_phone,
                      academic_coordination_email: data.academic_coordination_email,
                      help_desk_person: data.help_desk_person,
                      help_desk_phone: data.help_desk_phone,
                      platform: data_event.platform,
                      program: data.title,
                      sub_title: data.sub_title,
                      sessions: data.sessions,
                      id: data.id,
                      _id: data._id,
                      color: data.color,
                      background: data.background,
                      description: data_event.description,
                      place: data_event.place,
                      text: data_event.text,
                      title: data_event.title,
                      date: start_new_format_date[0],
                      star_hours: start_new_format_date[1],
                      star_hours_number: start_new_format_date[2],
                      end_hours: "11:59 pm",
                      end_hours_number: "23:59",
                       color_title: data.color_title
                    });
                  } else if (index_difference == difference - 1) {
                    date_new =  moment(data_new[data_new.length - 1].date, "YYYY-MM-DD")
                        .add(1, "day")
                        .format("YYYY-MM-DD")
                    name =  days_names[moment(date_new, "YYYY-MM-DD").day()]
                    name =  name.charAt(0).toUpperCase() + name.slice(1)
                    data_new.push({
                      name: name,
                      classroom: data_event.classroom,
                      eventid: data_event.id,
                      responsable_id: data.responsable_id,
                      program_dstart:  moment.unix( data.program_dstart ).format("DD/MM/YYYY"), 
                      program_dend:  moment.unix( data.program_dend ).format("DD/MM/YYYY"),
                      course_dstart: moment.unix( data.course_dstart ).format("DD/MM/YYYY"),
                      course_dend: moment.unix( data.course_dend ).format("DD/MM/YYYY"),
                      type: data.type,
                      modality: data_event.modality,
                      docente: data.docente,
                      academic_coordination_person: data.academic_coordination_person,
                      academic_coordination_phone: data.academic_coordination_phone,
                      academic_coordination_email: data.academic_coordination_email,
                      help_desk_person: data.help_desk_person,
                      help_desk_phone: data.help_desk_phone,
                      program: data.title,
                      platform: data_event.platform,
                      sub_title: data.sub_title,
                      sessions: data.sessions,  
                      id: data.id,
                      _id: data._id,
                      color: data.color,
                      background: data.background,
                      description: data_event.description,
                      place: data_event.place,
                      text: data_event.text,
                      title: data_event.title,
                      date: date_new,
                      star_hours: "0:00 am",
                      star_hours_number: "0:00",
                      end_hours: end_new_format_date[1],
                      end_hours_number: end_new_format_date[2],
                      color_title: data.color_title
                    });
                  } else {
                    date_new = moment(data_new[data_new.length - 1].date, "YYYY-MM-DD")
                                 .add(1, "day")
                                 .format("YYYY-MM-DD")
                    name = days_names[moment(date_new, "YYYY-MM-DD").day()],
                    name =  name.charAt(0).toUpperCase() + name.slice(1)
                    data_new.push({
                      name: name,
                      classroom: data_event.classroom,
                      eventid: data_event.id,
                      responsable_id: data.responsable_id,
                      program_dstart:  moment.unix( data.program_dstart ).format("DD/MM/YYYY"), 
                      program_dend:  moment.unix( data.program_dend ).format("DD/MM/YYYY"),
                      course_dstart: moment.unix( data.course_dstart ).format("DD/MM/YYYY"),
                      course_dend: moment.unix( data.course_dend ).format("DD/MM/YYYY"),
                      type: data.type,
                      modality: data_event.modality,
                      docente: data.docente,
                      academic_coordination_person: data.academic_coordination_person,
                      academic_coordination_phone: data.academic_coordination_phone,
                      academic_coordination_email: data.academic_coordination_email,
                      help_desk_person: data.help_desk_person,
                      help_desk_phone: data.help_desk_phone,
                      program: data.title,
                      platform: data_event.platform,
                      sub_title: data.sub_title,
                      sessions: data.sessions,  
                      id: data.id,
                      _id: data._id,
                      color: data.color,
                      background: data.background,
                      description: data_event.description,
                      place: data_event.place,
                      text: data_event.text,
                      title: data_event.title,
                      date: date_new,
                      star_hours: "0:00 am",
                      star_hours_number: "0:00",
                      end_hours: "23:59 pm",
                      end_hours_number: "23:59",
                       color_title: data.color_title
                    });
                  }
                }
              } else {
                data_new.push({
                  name: name,
                  classroom: data_event.classroom,
                  eventid: data_event.id,
                  responsable_id: data.responsable_id,
                  program_dstart:  moment.unix( data.program_dstart ).format("DD/MM/YYYY"), 
                  program_dend:  moment.unix( data.program_dend ).format("DD/MM/YYYY"),
                  course_dstart: moment.unix( data.course_dstart ).format("DD/MM/YYYY"),
                  course_dend: moment.unix( data.course_dend ).format("DD/MM/YYYY"),
                  type: data.type,
                  modality: data_event.modality,
                  docente: data.docente,
                  academic_coordination_person: data.academic_coordination_person,
                  academic_coordination_phone: data.academic_coordination_phone,
                  academic_coordination_email: data.academic_coordination_email,
                  help_desk_person: data.help_desk_person,
                  help_desk_phone: data.help_desk_phone,
                  program: data.title,
                  platform: data_event.platform,
                  sub_title: data.sub_title,
                  sessions: data.sessions,  
                  id: data.id,
                  _id: data._id,
                  color: data.color,
                  background: data.background,
                  description: data_event.description,
                  place: data_event.place,
                  text: data_event.text,
                  title: data_event.title,
                  date: start_new_format_date[0],
                  star_hours: start_new_format_date[1],
                  star_hours_number: start_new_format_date[2],
                  end_hours: end_new_format_date[1],
                  end_hours_number: end_new_format_date[2],
                  color_title: data.color_title
                });
              }
            }
          }
        }
  
        // Ordenar data por fecha
        data_new = data_new.sort(function(left, right) {
          return moment.utc(left.date).diff(moment.utc(right.date));
        });
        // end ordernar data
  
        vm.calendar_data_new = [...data_new];
        if ( vm.type == 'month' ) {
           vm.compare(await vm.getdaysArrayByMonth(vm.mmyy), data_new);
        } else {
            vm.weekCompare(await vm.getdaysArrayByWeek(vm.ddmmyy), data_new);
          
        }
       
      },
  
      async setMonth(state) {
        var vm = this;
        vm.$moment.locale("es");
        vm.state_menu = false;
        vm.state_menu_week = false;
        let param = [];
        if (vm.mode == "month") {
          if (vm.type == "month") {
            if (state) {
              vm.mmyy = vm
                .$moment("1-" + vm.mmyy, "DD-MM-YYYY")
                .add(1, "month")
                .format("MM-YYYY");
              param = vm.mmyy.split("-");
            } else {
              vm.mmyy = vm
                .$moment("1-" + vm.mmyy, "DD-MM-YYYY")
                .add(-1, "month")
                .format("MM-YYYY");
              param = vm.mmyy.split("-");
            }
            vm.month_name = vm.months[parseInt(param[0]) - 1];
            vm.month_name_minimun = vm.months_minimun[parseInt(param[0]) - 1];
            vm.month_label = vm.month_name + " de " + param[1];
  
            await vm.getdaysArrayByMonth(vm.mmyy);
            vm.searchText(vm.search);
          } else {
            if (state) {
              vm.ddmmyy = vm
                .$moment(vm.ddmmyy, "DD-MM-YYYY")
                .add(1, "week")
                .format("DD-MM-YYYY");
              vm.mmyy = vm
                .$moment(vm.ddmmyy, "DD-MM-YYYY")
                .add(1, "week")
                .format("MM-YYYY");
              param = vm.ddmmyy.split("-");
            } else {
              vm.ddmmyy = vm
                .$moment(vm.ddmmyy, "DD-MM-YYYY")
                .add(-1, "week")
                .format("DD-MM-YYYY");
              vm.mmyy = vm
                .$moment(vm.ddmmyy, "DD-MM-YYYY")
                .add(-1, "week")
                .format("MM-YYYY");
              param = vm.ddmmyy.split("-");
            }
  
            vm.month_name = vm.months[parseInt(param[1]) - 1];
            vm.month_name_minimun = vm.months_minimun[parseInt(param[1]) - 1];
            vm.month_label = vm.month_name + " de " + param[2];
  
            await vm.getdaysArrayByWeek(vm.ddmmyy);
            vm.searchTextWeek(vm.search);
          }
        } else {
          if (state) {
            vm.mmyy = vm
              .$moment("1-" + vm.mmyy, "DD-MM-YYYY")
              .add(1, "month")
              .format("MM-YYYY");
            param = vm.mmyy.split("-");
          } else {
            vm.mmyy = vm
              .$moment("1-" + vm.mmyy, "DD-MM-YYYY")
              .add(-1, "month")
              .format("MM-YYYY");
            param = vm.mmyy.split("-");
          }
          vm.month_name = vm.months[parseInt(param[0]) - 1];
          vm.month_name_minimun = vm.months_minimun[parseInt(param[0]) - 1];
          vm.month_label = vm.month_name + " de " + param[1];
          vm.setDataList(vm.mmyy);
        }
      },
  
      weekCompare(dataWeek, data) {
      var vm = this;
      moment.locale("es");
      let dataWeek_new = [...dataWeek];
      vm.state_data = false;
      dataWeek_new.forEach((element, index) => {
        let data_push = [];
        data.forEach(elementData => {
          elementData.width = 100
          let state = moment(element.date, "DD-MM-YYYY")
            .isSame(elementData.date, "DD-MM-YYYY");
          if (state) {
            //pertenece al día
            let star_hours_number = elementData.star_hours_number.split(":");
            let end_hours_number = elementData.end_hours_number.split(":");
            elementData.star_date_px = parseInt(star_hours_number[0]) * 30 + (parseInt(star_hours_number[1]) / 60) * 30; 
            elementData.end_hours_number_px = parseInt(end_hours_number[0]) * 30 + (parseInt(end_hours_number[1]) / 60) * 30 - elementData.star_date_px;
            data_push.push(elementData);
          }
        });
        dataWeek_new[index].data = data_push;
      });

        dataWeek_new.forEach(elementX => { //semana
        let data = []
        elementX.data.forEach((elementY,index) => { //día
        dataWeek_new.forEach(elementZ => { //comparativa
          elementZ.data.forEach(elementO => { 
            if ( elementY.star_date_px ==  elementO.star_date_px && elementY.end_hours_number_px == elementO.end_hours_number_px) {
                data.push(index)
            } else if ( elementY.star_date_px > elementO.star_date_px &&  elementY.star_date_px < (elementO.star_date_px + elementO.end_hours_number_px)) {
                elementY.width = elementY.width - 2 
            }
          });
        })
        
      })

      let data_format = []
      data.forEach(dataElemX => {
        let count = 0
        data.forEach(dataElemY => {
          if ( dataElemX == dataElemY) {
              count += 1
          }

        });
        if (count > 1) {
          data_format.push(dataElemX)
        }
      });

      data_format = [...new Set(data_format)]  
       
      data_format.forEach((elementData ,indexdata) => {
        elementX.data[elementData].width -= (indexdata +2)
      });
      

      
      
    })
 
      vm.days_array_week = dataWeek_new;
      vm.state_data = true;
    },
  
      compare(dataMonth, data) {
        var vm = this;
        vm.$moment.locale("es");
        vm.state_data = false;
  
        let dataMonth_new = [...dataMonth];
  
        dataMonth_new.forEach((element, index) => {
          let data_push = [];
          for (let data_index = 0; data_index < data.length; data_index++) {
            let state = vm
              .$moment(element.date, "DD-MM-YYYY")
              .isSame(data[data_index].date, "DD-MM-YYYY");
            if (state) {
              data_push.push(data[data_index]);
            }
          }
  
          if (data_push.length > 2) {
            element.data = [data_push[0], data_push[1]];
            element.more_than_two = true;
            element.count = data_push.length - 2;
            let ids_data = [];
  
            for (
              let index_push = 1;
              index_push < data_push.length;
              index_push++
            ) {
              ids_data.push(data_push[index_push]);
            }
  
            element.data[1].ids_data = ids_data;
          } else {
            dataMonth_new[index].data = [...data_push];
          }
        });
  
        vm.days_array = dataMonth_new;
        vm.state_data = true;
      },
  
      searchText(param) {
        var vm = this;
        vm.state_menu = false;
        vm.state_menu_week = false;
        vm.$moment.locale("es");
        let data = vm.calendar_data_new.filter(function(item) {
          if (
            item.text.toLowerCase().search(param) != -1 ||
            item.title.toLowerCase().search(param) != -1 ||
            item.date.toLowerCase().search(param) != -1 ||
            item.star_hours.toLowerCase().search(param) != -1 ||
            item.end_hours.toLowerCase().search(param) != -1
          ) {
            return item;
          }
        });
        vm.compare(vm.calendar_dataMonth, data);
      },
  
      searchTextWeek(param) {
        var vm = this;
        vm.state_menu = false;
        vm.state_menu_week = false;
        vm.$moment.locale("es");
        let data = vm.calendar_data_new.filter(function(item) {
          if (
            item.text.toLowerCase().search(param) != -1 ||
            item.title.toLowerCase().search(param) != -1 ||
            item.date.toLowerCase().search(param) != -1 ||
            item.star_hours.toLowerCase().search(param) != -1 ||
            item.end_hours.toLowerCase().search(param) != -1
          ) {
            return item;
          }
        });
        vm.weekCompare(vm.days_array_week, data);
      },
  
      saveSubMenu(data){
         this.$emit("saveSubMenu", data)
      },
      
       alertSubMenu(data){
         this.$emit("alertSubMenu", data)
      },
  
      synchronizeCalendar(){
          this.$emit("synchronizeCalendar")
      }
    }
}
 
