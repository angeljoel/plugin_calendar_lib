const templateCard = `
<div   @click.prevent="getMenu()"
                    :style="getStyle"   >
                    
                <div style="align-self: center;">
                    
                <div :class="[index == 1 ? 'text-uppercase font-title  header-desktop-title color-count-title' : 
                                            'text-uppercase font-title  header-desktop-title ' ]" 
                        > 
                        <span class="text-title-card-overflow break-word" 
                        :style="{'color': color}" v-html="param.title"></span>
                        <div v-show="more_than_two && index == 1" class="center-count  "
                        >+{{count}}</div>
                    </div>
                    <div class="header-desktop-text">
                    <div class="label-body-date break-word " >
                        {{param.text}}
                        <div  class="center-count  "
                        v-show="more_than_two && index == 1">+{{count}}</div>
                    </div>
                    </div>
                    <div class="header-desktop-hours" style="font-size:10px">{{param.star_hours}}</div>
                </div> 
                </div>`


const optionsCard =
{
    template: templateCard,
    props: {
        param: Object,
        index_column: Number,
        count: Number,
        more_than_two: Boolean,
        index: Number,
        color: String,
        background_days: Boolean,
        background: String,
        state_end_date: {
            default: false,
            type: Boolean
        }
    },

    computed: {
        getStyle() {
            var vm = this

            if (vm.background_days) {
                return `border: 1px solid  ${vm.color};
                                    border-radius: 0.12rem;
                                    border-left: 0.15rem solid ${vm.color};
                                    background: ${vm.background}`
            } else {
                return `border: 1px solid  ${vm.color};
                                    border-radius: 0.12rem;
                                    border-left: 0.15rem solid ${vm.color};
                                    background: #FFF`

            }
        }
    },
    data() {
        return {

        }
    },
    methods: {
        getTitleDesktop(text) {
            if (text != null && text != undefined && text != '') {
                if (text.length > 7) {
                    return text.substr(0, 6) + '...'
                } else {
                    return text
                }

            }
        },

        getTitleMovil(text) {
            if (text != null && text != undefined && text != '') {
                if (text.length > 7) {
                    return text.substr(0, 6) + '...'
                } else {
                    return text
                }

            }
        },

        getMenu() {
            var vm = this
            let param = {
                index: vm.index,
                index_column: vm.index_column
            }
            vm.$emit('getMenu', param)


        }
    }
}